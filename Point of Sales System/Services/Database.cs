﻿using System.Data;
using System.Data.SqlClient;
using Point_of_Sales_System.Model;
using Point_of_Sales_System.Properties;

namespace Point_of_Sales_System.Services;

public class Database
{
    private SqlDataAdapter _adapter = new();
    private SqlCommand _command;
    private readonly SqlConnection _conn;
    private DataSet _dataset;
    private SqlDataReader _sqlDataReader;

    public Database()
    {
        try
        {
            _conn = new SqlConnection(Resources._connectionString);
            _conn.Open();
            hasLoginError = false;
            _conn.Close();
        }
        catch (Exception e)
        {
            hasLoginError = true;
        }
    }


    public bool hasLoginError { get; set; }

    public string Query { get; set; }

    public double returnTotalSale()
    {
        double totalSale = 0;
        try
        {
            _conn.Open();
            _command = new SqlCommand(Query, _conn);
            _sqlDataReader = _command.ExecuteReader();
            if (!_sqlDataReader.HasRows)
            {
                _sqlDataReader.Close();
                _command.Dispose();
                _conn.Close();
                return 0;
            }

            _sqlDataReader.Read();
            totalSale = Convert.ToDouble(_sqlDataReader.GetValue(0));
        }
        catch (Exception e)
        {
            totalSale = 0;
        }
        finally
        {
            _sqlDataReader.Close();
            _command.Dispose();
            _conn.Close();
        }


        return totalSale;
    }

    public Employee GetEmployee(string table)
    {
        var employee = new Employee();
        _conn.Open();
        _command = new SqlCommand(Query, _conn);
        _sqlDataReader = _command.ExecuteReader();


        if (!_sqlDataReader.HasRows)
        {
            _sqlDataReader.Close();
            _command.Dispose();
            _conn.Close();
            return null;
        }

        _sqlDataReader.Read();

        if (table.Equals("EmployeeCredential"))
        {
            employee.UserName = _sqlDataReader.GetValue(0) + "";
            employee.Password = _sqlDataReader.GetValue(1) + "";
            employee.IsLoggedIn = Convert.ToBoolean(_sqlDataReader.GetValue(2) + "");
        }

        _sqlDataReader.Close();
        _command.Dispose();
        _conn.Close();
        return employee;
    }

    public DataSet getTableData(string table)
    {
        //Steps to fetch data from this method
        //1. Give the query
        //2. get the dataset 
        //3. select the table that needed to show
        _conn.Open();
        _adapter = new SqlDataAdapter(Query, _conn);

        _dataset = new DataSet();
        _adapter.Fill(_dataset, table);
        _conn.Close();
        return _dataset;
    }

    public void WriteXml(string path)
    {
        _dataset.WriteXml(path);
    }

    public Product getProductRecord()
    {
        var data = "";
        var product = new Product();
        _conn.Open();
        _command = new SqlCommand(Query, _conn);
        _sqlDataReader = _command.ExecuteReader();

        if (_sqlDataReader.HasRows)
        {
            _sqlDataReader.Read();

            for (var i = 0; i < _sqlDataReader.FieldCount; i++)
            {
                product.Id = Convert.ToInt32(_sqlDataReader.GetValue(0) + "");
                product.ProductPicture = _sqlDataReader.GetValue(1) + "";
                product.ProductName = _sqlDataReader.GetValue(2) + "";
                product.Price = Convert.ToDouble(_sqlDataReader.GetValue(3) + "");
                product.Stocks = Convert.ToInt32(_sqlDataReader.GetValue(4) + "");
                product.Description = _sqlDataReader.GetValue(5) + "";
                product.Expiration_Date = _sqlDataReader.GetValue(6) + "";
                product.CategoryName = _sqlDataReader.GetValue(7) + "";
            }
        }

        _conn.Close();
        _command.Dispose();
        _sqlDataReader.Close();
        return product;
    }


    public void AddUpdateDeleteRecord()
    {
        //this method provides the CRUD Functionality ng program
        //Steps to use:
        // 1: make a query Ex: _db.Query = $@"";
        // 2: call this function
        _conn.Open();
        _command = new SqlCommand(Query, _conn);
        _command.ExecuteNonQuery();
        _conn.Close();
    }

    public string GetLatestRecord(int columnIndex)
    {
        var latestRecord = "";
        _conn.Open();
        _command = new SqlCommand(Query, _conn);
        _sqlDataReader = _command.ExecuteReader();

        _sqlDataReader.Read();
        latestRecord = _sqlDataReader.GetValue(columnIndex) + "";
        _conn.Close();
        _command.Dispose();
        _sqlDataReader.Close();
        return latestRecord;
    }

    public List<Employee> getEmployeeRecord()
    {
        var employees = new List<Employee>();
        _conn.Open();
        _command = new SqlCommand(Query, _conn);
        _sqlDataReader = _command.ExecuteReader();
        var data = "";
        if (_sqlDataReader.HasRows)
        {
            while (_sqlDataReader.Read())
            {
                var employee = new Employee();
                employee.Id = Convert.ToInt32(_sqlDataReader.GetValue(0));
                employee.ProfilePicture = _sqlDataReader.GetValue(1) + "";
                employee.FirstName = _sqlDataReader.GetValue(2) + "";
                employee.MiddleName = _sqlDataReader.GetValue(3) + "";
                employee.LastName = _sqlDataReader.GetValue(4) + "";
                employee.Address = _sqlDataReader.GetValue(5) + "";
                employee.Gender = _sqlDataReader.GetValue(6) + "";
                employee.BirthDate = _sqlDataReader.GetValue(7) + "";
                employee.UserName = _sqlDataReader.GetValue(8) + "";
                employee.EmployeeType = _sqlDataReader.GetValue(9) + "";
                employee.AccountStatus = _sqlDataReader.GetValue(10) + "";


                employees.Add(employee);
            }

            _command.Dispose();
            _sqlDataReader.Close();
            _conn.Close();
        }
        else
        {
            return null;
        }

        return employees;
    }
}
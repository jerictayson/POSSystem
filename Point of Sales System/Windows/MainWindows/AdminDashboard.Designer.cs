﻿using System.Windows.Forms;

namespace Point_of_Sales_System.Windows.MainWindows
{
    partial class AdminDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnViewInventoryChanges = new System.Windows.Forms.Button();
            this.btnViewAccountUpdate = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnUpdateInventory = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnViewTransaction = new System.Windows.Forms.Button();
            this.lblAdminName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlReport = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTotalSales = new System.Windows.Forms.Label();
            this.pnlAddUpdDelProductOption = new System.Windows.Forms.Panel();
            this.pnlAddUpdDelProductMessage = new System.Windows.Forms.Panel();
            this.lblProductStatus = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnAddNewProduct = new System.Windows.Forms.Button();
            this.btnDeleteProduct = new System.Windows.Forms.Button();
            this.pnlAddUpdOption = new System.Windows.Forms.Panel();
            this.pnlAddUpdMessage = new System.Windows.Forms.Panel();
            this.lblAddUpdText = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnAddNewAccount = new System.Windows.Forms.Button();
            this.btnDeleteAccount = new System.Windows.Forms.Button();
            this.pnlTransactionOption = new System.Windows.Forms.Panel();
            this.cmbShowOption = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvUpdatedCopy = new System.Windows.Forms.DataGridView();
            this.dgvLocalCopy = new System.Windows.Forms.DataGridView();
            this.txtCashierSearch = new System.Windows.Forms.TextBox();
            this.lblSearchCashier = new System.Windows.Forms.Label();
            this.dgvLeftTable = new System.Windows.Forms.DataGridView();
            this.lblLeftTable = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlRightTableOptions = new System.Windows.Forms.Panel();
            this.btnViewEmployeeDetails = new System.Windows.Forms.Button();
            this.lblLoginCount = new System.Windows.Forms.Label();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblRightTotalPrice = new System.Windows.Forms.Label();
            this.dgvRightTable = new System.Windows.Forms.DataGridView();
            this.lblRightTable = new System.Windows.Forms.Label();
            this.pnlAddUpdDeleteProduct = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtProductReason = new System.Windows.Forms.TextBox();
            this.cmbProductDate = new System.Windows.Forms.ComboBox();
            this.cmbProductMonth = new System.Windows.Forms.ComboBox();
            this.cmbProductYear = new System.Windows.Forms.ComboBox();
            this.btnAddNewCategory = new System.Windows.Forms.Button();
            this.txtProductDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCreateProduct = new System.Windows.Forms.Button();
            this.btnUpdateProduct = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtStocks = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbProductType = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.pnlAddSomething = new System.Windows.Forms.Panel();
            this.chcbxShowPassword = new System.Windows.Forms.CheckBox();
            this.cmbDate = new System.Windows.Forms.ComboBox();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.cmbBirthYear = new System.Windows.Forms.ComboBox();
            this.txtAccountReason = new System.Windows.Forms.TextBox();
            this.lblAccountReason = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdbFemale = new System.Windows.Forms.RadioButton();
            this.rdbMale = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rdbAccountDisable = new System.Windows.Forms.RadioButton();
            this.rdbAccountEnable = new System.Windows.Forms.RadioButton();
            this.btnCreateAccount = new System.Windows.Forms.Button();
            this.btnUpdateAccount = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbAccountType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMiddleName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pctrEmployee = new System.Windows.Forms.PictureBox();
            this.lblAddSomething = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tmrRefreshData = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlReport.SuspendLayout();
            this.pnlAddUpdDelProductOption.SuspendLayout();
            this.pnlAddUpdDelProductMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.pnlAddUpdOption.SuspendLayout();
            this.pnlAddUpdMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.pnlTransactionOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatedCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeftTable)).BeginInit();
            this.panel3.SuspendLayout();
            this.pnlRightTableOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRightTable)).BeginInit();
            this.pnlAddUpdDeleteProduct.SuspendLayout();
            this.pnlAddSomething.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Khaki;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnViewInventoryChanges);
            this.panel1.Controls.Add(this.btnViewAccountUpdate);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.btnUpdateInventory);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnViewTransaction);
            this.panel1.Controls.Add(this.lblAdminName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(-2, -5);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 616);
            this.panel1.TabIndex = 0;
            // 
            // btnViewInventoryChanges
            // 
            this.btnViewInventoryChanges.BackColor = System.Drawing.Color.LightGreen;
            this.btnViewInventoryChanges.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewInventoryChanges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewInventoryChanges.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewInventoryChanges.Location = new System.Drawing.Point(2, 488);
            this.btnViewInventoryChanges.Margin = new System.Windows.Forms.Padding(0);
            this.btnViewInventoryChanges.Name = "btnViewInventoryChanges";
            this.btnViewInventoryChanges.Size = new System.Drawing.Size(184, 53);
            this.btnViewInventoryChanges.TabIndex = 9;
            this.btnViewInventoryChanges.Text = "View Inventory Changes";
            this.btnViewInventoryChanges.UseVisualStyleBackColor = false;
            this.btnViewInventoryChanges.Click += new System.EventHandler(this.btnViewInventoryChanges_Click);
            // 
            // btnViewAccountUpdate
            // 
            this.btnViewAccountUpdate.BackColor = System.Drawing.Color.LightGreen;
            this.btnViewAccountUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewAccountUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAccountUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewAccountUpdate.Location = new System.Drawing.Point(2, 435);
            this.btnViewAccountUpdate.Margin = new System.Windows.Forms.Padding(0);
            this.btnViewAccountUpdate.Name = "btnViewAccountUpdate";
            this.btnViewAccountUpdate.Size = new System.Drawing.Size(185, 53);
            this.btnViewAccountUpdate.TabIndex = 8;
            this.btnViewAccountUpdate.Text = "View Account Updates";
            this.btnViewAccountUpdate.UseVisualStyleBackColor = false;
            this.btnViewAccountUpdate.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(0, 571);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(187, 43);
            this.button4.TabIndex = 7;
            this.button4.Text = "Logout";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnUpdateInventory
            // 
            this.btnUpdateInventory.BackColor = System.Drawing.Color.LightGreen;
            this.btnUpdateInventory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateInventory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateInventory.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpdateInventory.Location = new System.Drawing.Point(2, 379);
            this.btnUpdateInventory.Margin = new System.Windows.Forms.Padding(0);
            this.btnUpdateInventory.Name = "btnUpdateInventory";
            this.btnUpdateInventory.Size = new System.Drawing.Size(184, 56);
            this.btnUpdateInventory.TabIndex = 6;
            this.btnUpdateInventory.Text = "Update Inventory";
            this.btnUpdateInventory.UseVisualStyleBackColor = false;
            this.btnUpdateInventory.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightGreen;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(2, 322);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(186, 57);
            this.button2.TabIndex = 5;
            this.button2.Text = "Update User Account";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightGreen;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(2, 268);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 57);
            this.button1.TabIndex = 4;
            this.button1.Text = "View Employee Logins";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnViewTransaction
            // 
            this.btnViewTransaction.BackColor = System.Drawing.Color.LightGreen;
            this.btnViewTransaction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewTransaction.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewTransaction.Location = new System.Drawing.Point(2, 212);
            this.btnViewTransaction.Margin = new System.Windows.Forms.Padding(0);
            this.btnViewTransaction.Name = "btnViewTransaction";
            this.btnViewTransaction.Size = new System.Drawing.Size(185, 56);
            this.btnViewTransaction.TabIndex = 3;
            this.btnViewTransaction.Text = "View Transaction";
            this.btnViewTransaction.UseVisualStyleBackColor = false;
            this.btnViewTransaction.Click += new System.EventHandler(this.btnViewTransaction_Click);
            // 
            // lblAdminName
            // 
            this.lblAdminName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblAdminName.Location = new System.Drawing.Point(4, 162);
            this.lblAdminName.Name = "lblAdminName";
            this.lblAdminName.Size = new System.Drawing.Size(180, 41);
            this.lblAdminName.TabIndex = 2;
            this.lblAdminName.Text = "Welcome";
            this.lblAdminName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(55, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Point_of_Sales_System.Properties.Resources.user;
            this.pictureBox1.Location = new System.Drawing.Point(0, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(187, 96);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Khaki;
            this.panel2.Controls.Add(this.pnlReport);
            this.panel2.Controls.Add(this.pnlAddUpdDelProductOption);
            this.panel2.Controls.Add(this.pnlAddUpdOption);
            this.panel2.Controls.Add(this.pnlTransactionOption);
            this.panel2.Controls.Add(this.dgvUpdatedCopy);
            this.panel2.Controls.Add(this.dgvLocalCopy);
            this.panel2.Controls.Add(this.txtCashierSearch);
            this.panel2.Controls.Add(this.lblSearchCashier);
            this.panel2.Controls.Add(this.dgvLeftTable);
            this.panel2.Controls.Add(this.lblLeftTable);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel2.Location = new System.Drawing.Point(186, 1);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(534, 610);
            this.panel2.TabIndex = 8;
            // 
            // pnlReport
            // 
            this.pnlReport.Controls.Add(this.label3);
            this.pnlReport.Controls.Add(this.lblTotalSales);
            this.pnlReport.Location = new System.Drawing.Point(12, 552);
            this.pnlReport.Name = "pnlReport";
            this.pnlReport.Size = new System.Drawing.Size(510, 48);
            this.pnlReport.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(207, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "Total Sales for this Month:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalSales
            // 
            this.lblTotalSales.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblTotalSales.Location = new System.Drawing.Point(4, 23);
            this.lblTotalSales.Name = "lblTotalSales";
            this.lblTotalSales.Size = new System.Drawing.Size(177, 18);
            this.lblTotalSales.TabIndex = 11;
            this.lblTotalSales.Text = "P1.00";
            this.lblTotalSales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlAddUpdDelProductOption
            // 
            this.pnlAddUpdDelProductOption.Controls.Add(this.pnlAddUpdDelProductMessage);
            this.pnlAddUpdDelProductOption.Controls.Add(this.btnAddNewProduct);
            this.pnlAddUpdDelProductOption.Controls.Add(this.btnDeleteProduct);
            this.pnlAddUpdDelProductOption.Location = new System.Drawing.Point(9, 552);
            this.pnlAddUpdDelProductOption.Name = "pnlAddUpdDelProductOption";
            this.pnlAddUpdDelProductOption.Size = new System.Drawing.Size(522, 55);
            this.pnlAddUpdDelProductOption.TabIndex = 23;
            this.pnlAddUpdDelProductOption.Visible = false;
            // 
            // pnlAddUpdDelProductMessage
            // 
            this.pnlAddUpdDelProductMessage.Controls.Add(this.lblProductStatus);
            this.pnlAddUpdDelProductMessage.Controls.Add(this.pictureBox3);
            this.pnlAddUpdDelProductMessage.Location = new System.Drawing.Point(8, 6);
            this.pnlAddUpdDelProductMessage.Name = "pnlAddUpdDelProductMessage";
            this.pnlAddUpdDelProductMessage.Size = new System.Drawing.Size(211, 38);
            this.pnlAddUpdDelProductMessage.TabIndex = 14;
            this.pnlAddUpdDelProductMessage.Visible = false;
            // 
            // lblProductStatus
            // 
            this.lblProductStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductStatus.Location = new System.Drawing.Point(54, 4);
            this.lblProductStatus.Name = "lblProductStatus";
            this.lblProductStatus.Size = new System.Drawing.Size(154, 29);
            this.lblProductStatus.TabIndex = 23;
            this.lblProductStatus.Text = "Search Cashier";
            this.lblProductStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Point_of_Sales_System.Properties.Resources.check;
            this.pictureBox3.Location = new System.Drawing.Point(3, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(46, 29);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // btnAddNewProduct
            // 
            this.btnAddNewProduct.BackColor = System.Drawing.Color.LimeGreen;
            this.btnAddNewProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewProduct.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnAddNewProduct.Location = new System.Drawing.Point(224, 10);
            this.btnAddNewProduct.Name = "btnAddNewProduct";
            this.btnAddNewProduct.Size = new System.Drawing.Size(129, 33);
            this.btnAddNewProduct.TabIndex = 12;
            this.btnAddNewProduct.Text = "Add Product";
            this.btnAddNewProduct.UseVisualStyleBackColor = false;
            this.btnAddNewProduct.Click += new System.EventHandler(this.btnAddNewProduct_Click);
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.BackColor = System.Drawing.Color.Crimson;
            this.btnDeleteProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteProduct.Enabled = false;
            this.btnDeleteProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteProduct.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnDeleteProduct.Location = new System.Drawing.Point(359, 10);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(155, 31);
            this.btnDeleteProduct.TabIndex = 13;
            this.btnDeleteProduct.Text = "Delete Product";
            this.btnDeleteProduct.UseVisualStyleBackColor = false;
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // pnlAddUpdOption
            // 
            this.pnlAddUpdOption.Controls.Add(this.pnlAddUpdMessage);
            this.pnlAddUpdOption.Controls.Add(this.btnAddNewAccount);
            this.pnlAddUpdOption.Controls.Add(this.btnDeleteAccount);
            this.pnlAddUpdOption.Location = new System.Drawing.Point(9, 552);
            this.pnlAddUpdOption.Name = "pnlAddUpdOption";
            this.pnlAddUpdOption.Size = new System.Drawing.Size(522, 55);
            this.pnlAddUpdOption.TabIndex = 22;
            this.pnlAddUpdOption.Visible = false;
            // 
            // pnlAddUpdMessage
            // 
            this.pnlAddUpdMessage.Controls.Add(this.lblAddUpdText);
            this.pnlAddUpdMessage.Controls.Add(this.pictureBox4);
            this.pnlAddUpdMessage.Location = new System.Drawing.Point(8, 6);
            this.pnlAddUpdMessage.Name = "pnlAddUpdMessage";
            this.pnlAddUpdMessage.Size = new System.Drawing.Size(211, 38);
            this.pnlAddUpdMessage.TabIndex = 14;
            this.pnlAddUpdMessage.Visible = false;
            // 
            // lblAddUpdText
            // 
            this.lblAddUpdText.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddUpdText.Location = new System.Drawing.Point(54, 4);
            this.lblAddUpdText.Name = "lblAddUpdText";
            this.lblAddUpdText.Size = new System.Drawing.Size(154, 29);
            this.lblAddUpdText.TabIndex = 23;
            this.lblAddUpdText.Text = "Search Cashier";
            this.lblAddUpdText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Point_of_Sales_System.Properties.Resources.check;
            this.pictureBox4.Location = new System.Drawing.Point(3, 4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(46, 29);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // btnAddNewAccount
            // 
            this.btnAddNewAccount.BackColor = System.Drawing.Color.LimeGreen;
            this.btnAddNewAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewAccount.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnAddNewAccount.Location = new System.Drawing.Point(224, 10);
            this.btnAddNewAccount.Name = "btnAddNewAccount";
            this.btnAddNewAccount.Size = new System.Drawing.Size(129, 33);
            this.btnAddNewAccount.TabIndex = 12;
            this.btnAddNewAccount.Text = "Add Account";
            this.btnAddNewAccount.UseVisualStyleBackColor = false;
            this.btnAddNewAccount.Click += new System.EventHandler(this.btnAddNewAccount_Click);
            // 
            // btnDeleteAccount
            // 
            this.btnDeleteAccount.BackColor = System.Drawing.Color.Crimson;
            this.btnDeleteAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteAccount.Enabled = false;
            this.btnDeleteAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAccount.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnDeleteAccount.Location = new System.Drawing.Point(359, 10);
            this.btnDeleteAccount.Name = "btnDeleteAccount";
            this.btnDeleteAccount.Size = new System.Drawing.Size(155, 31);
            this.btnDeleteAccount.TabIndex = 13;
            this.btnDeleteAccount.Text = "Delete Account";
            this.btnDeleteAccount.UseVisualStyleBackColor = false;
            this.btnDeleteAccount.Click += new System.EventHandler(this.btnDeleteAccount_Click);
            // 
            // pnlTransactionOption
            // 
            this.pnlTransactionOption.Controls.Add(this.cmbShowOption);
            this.pnlTransactionOption.Controls.Add(this.label8);
            this.pnlTransactionOption.Location = new System.Drawing.Point(235, 67);
            this.pnlTransactionOption.Name = "pnlTransactionOption";
            this.pnlTransactionOption.Size = new System.Drawing.Size(288, 35);
            this.pnlTransactionOption.TabIndex = 20;
            // 
            // cmbShowOption
            // 
            this.cmbShowOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShowOption.FormattingEnabled = true;
            this.cmbShowOption.Location = new System.Drawing.Point(116, 10);
            this.cmbShowOption.Name = "cmbShowOption";
            this.cmbShowOption.Size = new System.Drawing.Size(171, 21);
            this.cmbShowOption.TabIndex = 17;
            this.cmbShowOption.SelectedIndexChanged += new System.EventHandler(this.cmbShowOption_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 29);
            this.label8.TabIndex = 16;
            this.label8.Text = "Show:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvUpdatedCopy
            // 
            this.dgvUpdatedCopy.AllowUserToAddRows = false;
            this.dgvUpdatedCopy.AllowUserToDeleteRows = false;
            this.dgvUpdatedCopy.AllowUserToResizeColumns = false;
            this.dgvUpdatedCopy.AllowUserToResizeRows = false;
            this.dgvUpdatedCopy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdatedCopy.Location = new System.Drawing.Point(481, 44);
            this.dgvUpdatedCopy.Name = "dgvUpdatedCopy";
            this.dgvUpdatedCopy.ReadOnly = true;
            this.dgvUpdatedCopy.RowTemplate.Height = 25;
            this.dgvUpdatedCopy.Size = new System.Drawing.Size(18, 17);
            this.dgvUpdatedCopy.TabIndex = 19;
            this.dgvUpdatedCopy.Visible = false;
            // 
            // dgvLocalCopy
            // 
            this.dgvLocalCopy.AllowUserToAddRows = false;
            this.dgvLocalCopy.AllowUserToDeleteRows = false;
            this.dgvLocalCopy.AllowUserToResizeColumns = false;
            this.dgvLocalCopy.AllowUserToResizeRows = false;
            this.dgvLocalCopy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocalCopy.Location = new System.Drawing.Point(504, 44);
            this.dgvLocalCopy.Name = "dgvLocalCopy";
            this.dgvLocalCopy.RowTemplate.Height = 25;
            this.dgvLocalCopy.Size = new System.Drawing.Size(18, 17);
            this.dgvLocalCopy.TabIndex = 18;
            this.dgvLocalCopy.Visible = false;
            // 
            // txtCashierSearch
            // 
            this.txtCashierSearch.Location = new System.Drawing.Point(351, 103);
            this.txtCashierSearch.Name = "txtCashierSearch";
            this.txtCashierSearch.Size = new System.Drawing.Size(172, 20);
            this.txtCashierSearch.TabIndex = 15;
            this.txtCashierSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCashierSearch_KeyUp);
            // 
            // lblSearchCashier
            // 
            this.lblSearchCashier.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchCashier.Location = new System.Drawing.Point(209, 99);
            this.lblSearchCashier.Name = "lblSearchCashier";
            this.lblSearchCashier.Size = new System.Drawing.Size(136, 30);
            this.lblSearchCashier.TabIndex = 14;
            this.lblSearchCashier.Text = "Search Employee";
            this.lblSearchCashier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvLeftTable
            // 
            this.dgvLeftTable.AllowUserToAddRows = false;
            this.dgvLeftTable.AllowUserToDeleteRows = false;
            this.dgvLeftTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLeftTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvLeftTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLeftTable.Location = new System.Drawing.Point(9, 138);
            this.dgvLeftTable.MultiSelect = false;
            this.dgvLeftTable.Name = "dgvLeftTable";
            this.dgvLeftTable.ReadOnly = true;
            this.dgvLeftTable.RowHeadersVisible = false;
            this.dgvLeftTable.RowTemplate.Height = 25;
            this.dgvLeftTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLeftTable.Size = new System.Drawing.Size(513, 406);
            this.dgvLeftTable.TabIndex = 9;
            this.dgvLeftTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLeftTable_CellClick);
            // 
            // lblLeftTable
            // 
            this.lblLeftTable.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftTable.Location = new System.Drawing.Point(8, 98);
            this.lblLeftTable.Name = "lblLeftTable";
            this.lblLeftTable.Size = new System.Drawing.Size(207, 31);
            this.lblLeftTable.TabIndex = 8;
            this.lblLeftTable.Text = "Employee\'s Order List";
            this.lblLeftTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightGreen;
            this.panel3.Controls.Add(this.pnlAddUpdDeleteProduct);
            this.panel3.Controls.Add(this.pnlRightTableOptions);
            this.panel3.Controls.Add(this.pnlAddSomething);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel3.Location = new System.Drawing.Point(720, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(579, 609);
            this.panel3.TabIndex = 9;
            // 
            // pnlRightTableOptions
            // 
            this.pnlRightTableOptions.Controls.Add(this.btnViewEmployeeDetails);
            this.pnlRightTableOptions.Controls.Add(this.lblLoginCount);
            this.pnlRightTableOptions.Controls.Add(this.lblTotalPrice);
            this.pnlRightTableOptions.Controls.Add(this.lblRightTotalPrice);
            this.pnlRightTableOptions.Controls.Add(this.dgvRightTable);
            this.pnlRightTableOptions.Controls.Add(this.lblRightTable);
            this.pnlRightTableOptions.Location = new System.Drawing.Point(18, 97);
            this.pnlRightTableOptions.Name = "pnlRightTableOptions";
            this.pnlRightTableOptions.Size = new System.Drawing.Size(543, 497);
            this.pnlRightTableOptions.TabIndex = 0;
            // 
            // btnViewEmployeeDetails
            // 
            this.btnViewEmployeeDetails.BackColor = System.Drawing.Color.LimeGreen;
            this.btnViewEmployeeDetails.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewEmployeeDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewEmployeeDetails.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewEmployeeDetails.Location = new System.Drawing.Point(14, 454);
            this.btnViewEmployeeDetails.Name = "btnViewEmployeeDetails";
            this.btnViewEmployeeDetails.Size = new System.Drawing.Size(187, 33);
            this.btnViewEmployeeDetails.TabIndex = 15;
            this.btnViewEmployeeDetails.Text = "View Employee Details";
            this.btnViewEmployeeDetails.UseVisualStyleBackColor = false;
            this.btnViewEmployeeDetails.Visible = false;
            this.btnViewEmployeeDetails.Click += new System.EventHandler(this.btnViewEmployeeDetails_Click);
            // 
            // lblLoginCount
            // 
            this.lblLoginCount.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblLoginCount.Location = new System.Drawing.Point(10, 29);
            this.lblLoginCount.Name = "lblLoginCount";
            this.lblLoginCount.Size = new System.Drawing.Size(161, 18);
            this.lblLoginCount.TabIndex = 19;
            this.lblLoginCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblTotalPrice.Location = new System.Drawing.Point(10, 475);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(207, 18);
            this.lblTotalPrice.TabIndex = 18;
            this.lblTotalPrice.Text = "P1.00";
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRightTotalPrice
            // 
            this.lblRightTotalPrice.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblRightTotalPrice.Location = new System.Drawing.Point(10, 449);
            this.lblRightTotalPrice.Name = "lblRightTotalPrice";
            this.lblRightTotalPrice.Size = new System.Drawing.Size(207, 18);
            this.lblRightTotalPrice.TabIndex = 17;
            this.lblRightTotalPrice.Text = "Total Price";
            this.lblRightTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvRightTable
            // 
            this.dgvRightTable.AllowUserToAddRows = false;
            this.dgvRightTable.AllowUserToDeleteRows = false;
            this.dgvRightTable.AllowUserToResizeRows = false;
            this.dgvRightTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRightTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRightTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRightTable.Location = new System.Drawing.Point(14, 50);
            this.dgvRightTable.MultiSelect = false;
            this.dgvRightTable.Name = "dgvRightTable";
            this.dgvRightTable.ReadOnly = true;
            this.dgvRightTable.RowHeadersVisible = false;
            this.dgvRightTable.RowTemplate.Height = 25;
            this.dgvRightTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRightTable.Size = new System.Drawing.Size(512, 396);
            this.dgvRightTable.TabIndex = 16;
            this.dgvRightTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRightTable_CellClick);
            // 
            // lblRightTable
            // 
            this.lblRightTable.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblRightTable.Location = new System.Drawing.Point(10, 7);
            this.lblRightTable.Name = "lblRightTable";
            this.lblRightTable.Size = new System.Drawing.Size(161, 18);
            this.lblRightTable.TabIndex = 16;
            this.lblRightTable.Text = "Transaction Details";
            this.lblRightTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlAddUpdDeleteProduct
            // 
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label16);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.txtProductReason);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.cmbProductDate);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.cmbProductMonth);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.cmbProductYear);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.btnAddNewCategory);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.txtProductDescription);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label6);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.btnCreateProduct);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.btnUpdateProduct);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label17);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.txtStocks);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label18);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.cmbProductType);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label19);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.txtPrice);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label20);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.txtProductName);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label25);
            this.pnlAddUpdDeleteProduct.Controls.Add(this.label26);
            this.pnlAddUpdDeleteProduct.Cursor = System.Windows.Forms.Cursors.Default;
            this.pnlAddUpdDeleteProduct.Location = new System.Drawing.Point(18, 89);
            this.pnlAddUpdDeleteProduct.Name = "pnlAddUpdDeleteProduct";
            this.pnlAddUpdDeleteProduct.Size = new System.Drawing.Size(543, 510);
            this.pnlAddUpdDeleteProduct.TabIndex = 50;
            this.pnlAddUpdDeleteProduct.Visible = false;
            this.pnlAddUpdDeleteProduct.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAddUpdDeleteProduct_Paint);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(287, 244);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(216, 23);
            this.label16.TabIndex = 55;
            this.label16.Text = "Reason";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProductReason
            // 
            this.txtProductReason.Location = new System.Drawing.Point(276, 266);
            this.txtProductReason.Multiline = true;
            this.txtProductReason.Name = "txtProductReason";
            this.txtProductReason.Size = new System.Drawing.Size(258, 180);
            this.txtProductReason.TabIndex = 54;
            // 
            // cmbProductDate
            // 
            this.cmbProductDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductDate.FormattingEnabled = true;
            this.cmbProductDate.Location = new System.Drawing.Point(128, 198);
            this.cmbProductDate.Name = "cmbProductDate";
            this.cmbProductDate.Size = new System.Drawing.Size(40, 21);
            this.cmbProductDate.TabIndex = 53;
            this.cmbProductDate.SelectedIndexChanged += new System.EventHandler(this.cmbProductDate_SelectedIndexChanged);
            // 
            // cmbProductMonth
            // 
            this.cmbProductMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductMonth.FormattingEnabled = true;
            this.cmbProductMonth.Location = new System.Drawing.Point(74, 198);
            this.cmbProductMonth.Name = "cmbProductMonth";
            this.cmbProductMonth.Size = new System.Drawing.Size(48, 21);
            this.cmbProductMonth.TabIndex = 52;
            this.cmbProductMonth.SelectedIndexChanged += new System.EventHandler(this.cmbProductMonth_SelectedIndexChanged);
            // 
            // cmbProductYear
            // 
            this.cmbProductYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductYear.FormattingEnabled = true;
            this.cmbProductYear.Location = new System.Drawing.Point(12, 198);
            this.cmbProductYear.Name = "cmbProductYear";
            this.cmbProductYear.Size = new System.Drawing.Size(56, 21);
            this.cmbProductYear.TabIndex = 51;
            this.cmbProductYear.SelectedIndexChanged += new System.EventHandler(this.cmbProductYear_SelectedIndexChanged);
            // 
            // btnAddNewCategory
            // 
            this.btnAddNewCategory.BackColor = System.Drawing.Color.LimeGreen;
            this.btnAddNewCategory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewCategory.Location = new System.Drawing.Point(422, 138);
            this.btnAddNewCategory.Name = "btnAddNewCategory";
            this.btnAddNewCategory.Size = new System.Drawing.Size(114, 27);
            this.btnAddNewCategory.TabIndex = 50;
            this.btnAddNewCategory.Text = "Add new Category";
            this.btnAddNewCategory.UseVisualStyleBackColor = false;
            this.btnAddNewCategory.Click += new System.EventHandler(this.btnAddNewCategory_Click);
            // 
            // txtProductDescription
            // 
            this.txtProductDescription.Location = new System.Drawing.Point(12, 265);
            this.txtProductDescription.Multiline = true;
            this.txtProductDescription.Name = "txtProductDescription";
            this.txtProductDescription.Size = new System.Drawing.Size(258, 181);
            this.txtProductDescription.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(10, 239);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(216, 23);
            this.label6.TabIndex = 49;
            this.label6.Text = "Description";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCreateProduct
            // 
            this.btnCreateProduct.BackColor = System.Drawing.Color.LimeGreen;
            this.btnCreateProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreateProduct.Enabled = false;
            this.btnCreateProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateProduct.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnCreateProduct.Location = new System.Drawing.Point(258, 470);
            this.btnCreateProduct.Name = "btnCreateProduct";
            this.btnCreateProduct.Size = new System.Drawing.Size(138, 31);
            this.btnCreateProduct.TabIndex = 14;
            this.btnCreateProduct.Text = "Create Product";
            this.btnCreateProduct.UseVisualStyleBackColor = false;
            this.btnCreateProduct.Click += new System.EventHandler(this.btnCreateProduct_Click);
            // 
            // btnUpdateProduct
            // 
            this.btnUpdateProduct.BackColor = System.Drawing.Color.Yellow;
            this.btnUpdateProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateProduct.Enabled = false;
            this.btnUpdateProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateProduct.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpdateProduct.Location = new System.Drawing.Point(402, 470);
            this.btnUpdateProduct.Name = "btnUpdateProduct";
            this.btnUpdateProduct.Size = new System.Drawing.Size(138, 31);
            this.btnUpdateProduct.TabIndex = 14;
            this.btnUpdateProduct.Text = "Update";
            this.btnUpdateProduct.UseVisualStyleBackColor = false;
            this.btnUpdateProduct.Click += new System.EventHandler(this.btnUpdateProduct_Click);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(4, 173);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(216, 18);
            this.label17.TabIndex = 42;
            this.label17.Text = "Expiration Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtStocks
            // 
            this.txtStocks.Location = new System.Drawing.Point(287, 221);
            this.txtStocks.Name = "txtStocks";
            this.txtStocks.Size = new System.Drawing.Size(80, 20);
            this.txtStocks.TabIndex = 39;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(287, 200);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(216, 18);
            this.label18.TabIndex = 40;
            this.label18.Text = "Stocks";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbProductType
            // 
            this.cmbProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProductType.FormattingEnabled = true;
            this.cmbProductType.Location = new System.Drawing.Point(287, 173);
            this.cmbProductType.Name = "cmbProductType";
            this.cmbProductType.Size = new System.Drawing.Size(249, 21);
            this.cmbProductType.TabIndex = 38;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(283, 123);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 42);
            this.label19.TabIndex = 37;
            this.label19.Text = "Product Category";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(9, 142);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(72, 20);
            this.txtPrice.TabIndex = 35;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(5, 121);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(207, 18);
            this.label20.TabIndex = 36;
            this.label20.Text = "Price";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtProductName
            // 
            this.txtProductName.Location = new System.Drawing.Point(9, 95);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(257, 20);
            this.txtProductName.TabIndex = 22;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(5, 67);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(207, 18);
            this.label25.TabIndex = 25;
            this.label25.Text = "Product Name";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(4, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(305, 50);
            this.label26.TabIndex = 22;
            this.label26.Text = "Please Select an account or click add new user account";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlAddSomething
            // 
            this.pnlAddSomething.Controls.Add(this.chcbxShowPassword);
            this.pnlAddSomething.Controls.Add(this.cmbDate);
            this.pnlAddSomething.Controls.Add(this.cmbMonth);
            this.pnlAddSomething.Controls.Add(this.cmbBirthYear);
            this.pnlAddSomething.Controls.Add(this.txtAccountReason);
            this.pnlAddSomething.Controls.Add(this.lblAccountReason);
            this.pnlAddSomething.Controls.Add(this.panel5);
            this.pnlAddSomething.Controls.Add(this.panel4);
            this.pnlAddSomething.Controls.Add(this.btnCreateAccount);
            this.pnlAddSomething.Controls.Add(this.btnUpdateAccount);
            this.pnlAddSomething.Controls.Add(this.label15);
            this.pnlAddSomething.Controls.Add(this.txtPassword);
            this.pnlAddSomething.Controls.Add(this.label14);
            this.pnlAddSomething.Controls.Add(this.txtUserName);
            this.pnlAddSomething.Controls.Add(this.label13);
            this.pnlAddSomething.Controls.Add(this.cmbAccountType);
            this.pnlAddSomething.Controls.Add(this.label12);
            this.pnlAddSomething.Controls.Add(this.label11);
            this.pnlAddSomething.Controls.Add(this.txtAddress);
            this.pnlAddSomething.Controls.Add(this.label10);
            this.pnlAddSomething.Controls.Add(this.label9);
            this.pnlAddSomething.Controls.Add(this.txtLastName);
            this.pnlAddSomething.Controls.Add(this.label7);
            this.pnlAddSomething.Controls.Add(this.txtMiddleName);
            this.pnlAddSomething.Controls.Add(this.label5);
            this.pnlAddSomething.Controls.Add(this.txtFirstName);
            this.pnlAddSomething.Controls.Add(this.label2);
            this.pnlAddSomething.Controls.Add(this.pctrEmployee);
            this.pnlAddSomething.Controls.Add(this.lblAddSomething);
            this.pnlAddSomething.Cursor = System.Windows.Forms.Cursors.Default;
            this.pnlAddSomething.Location = new System.Drawing.Point(18, 90);
            this.pnlAddSomething.Name = "pnlAddSomething";
            this.pnlAddSomething.Size = new System.Drawing.Size(543, 510);
            this.pnlAddSomething.TabIndex = 9;
            this.pnlAddSomething.Visible = false;
            // 
            // chcbxShowPassword
            // 
            this.chcbxShowPassword.AutoSize = true;
            this.chcbxShowPassword.Location = new System.Drawing.Point(287, 176);
            this.chcbxShowPassword.Name = "chcbxShowPassword";
            this.chcbxShowPassword.Size = new System.Drawing.Size(102, 17);
            this.chcbxShowPassword.TabIndex = 53;
            this.chcbxShowPassword.Text = "Show Password";
            this.chcbxShowPassword.UseVisualStyleBackColor = true;
            this.chcbxShowPassword.CheckedChanged += new System.EventHandler(this.chcbxShowPassword_CheckedChanged);
            // 
            // cmbDate
            // 
            this.cmbDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDate.FormattingEnabled = true;
            this.cmbDate.Location = new System.Drawing.Point(116, 401);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Size = new System.Drawing.Size(40, 21);
            this.cmbDate.TabIndex = 52;
            // 
            // cmbMonth
            // 
            this.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Location = new System.Drawing.Point(77, 401);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(33, 21);
            this.cmbMonth.TabIndex = 51;
            this.cmbMonth.SelectedIndexChanged += new System.EventHandler(this.cmbMonth_SelectedIndexChanged);
            // 
            // cmbBirthYear
            // 
            this.cmbBirthYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBirthYear.FormattingEnabled = true;
            this.cmbBirthYear.Location = new System.Drawing.Point(14, 401);
            this.cmbBirthYear.Name = "cmbBirthYear";
            this.cmbBirthYear.Size = new System.Drawing.Size(57, 21);
            this.cmbBirthYear.TabIndex = 50;
            this.cmbBirthYear.SelectedIndexChanged += new System.EventHandler(this.cmbBirthYear_SelectedIndexChanged);
            // 
            // txtAccountReason
            // 
            this.txtAccountReason.Location = new System.Drawing.Point(287, 264);
            this.txtAccountReason.Multiline = true;
            this.txtAccountReason.Name = "txtAccountReason";
            this.txtAccountReason.Size = new System.Drawing.Size(253, 200);
            this.txtAccountReason.TabIndex = 48;
            // 
            // lblAccountReason
            // 
            this.lblAccountReason.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblAccountReason.Location = new System.Drawing.Point(283, 239);
            this.lblAccountReason.Name = "lblAccountReason";
            this.lblAccountReason.Size = new System.Drawing.Size(216, 23);
            this.lblAccountReason.TabIndex = 49;
            this.lblAccountReason.Text = "Reason:";
            this.lblAccountReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rdbFemale);
            this.panel5.Controls.Add(this.rdbMale);
            this.panel5.Location = new System.Drawing.Point(83, 296);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(146, 33);
            this.panel5.TabIndex = 47;
            // 
            // rdbFemale
            // 
            this.rdbFemale.AutoSize = true;
            this.rdbFemale.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.rdbFemale.Location = new System.Drawing.Point(78, 3);
            this.rdbFemale.Name = "rdbFemale";
            this.rdbFemale.Size = new System.Drawing.Size(65, 19);
            this.rdbFemale.TabIndex = 32;
            this.rdbFemale.TabStop = true;
            this.rdbFemale.Text = "Female";
            this.rdbFemale.UseVisualStyleBackColor = true;
            // 
            // rdbMale
            // 
            this.rdbMale.AutoSize = true;
            this.rdbMale.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.rdbMale.Location = new System.Drawing.Point(6, 3);
            this.rdbMale.Name = "rdbMale";
            this.rdbMale.Size = new System.Drawing.Size(52, 19);
            this.rdbMale.TabIndex = 31;
            this.rdbMale.TabStop = true;
            this.rdbMale.Text = "Male";
            this.rdbMale.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.rdbAccountDisable);
            this.panel4.Controls.Add(this.rdbAccountEnable);
            this.panel4.Location = new System.Drawing.Point(382, 206);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(140, 28);
            this.panel4.TabIndex = 46;
            // 
            // rdbAccountDisable
            // 
            this.rdbAccountDisable.AutoSize = true;
            this.rdbAccountDisable.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.rdbAccountDisable.Location = new System.Drawing.Point(72, 6);
            this.rdbAccountDisable.Name = "rdbAccountDisable";
            this.rdbAccountDisable.Size = new System.Drawing.Size(65, 19);
            this.rdbAccountDisable.TabIndex = 45;
            this.rdbAccountDisable.TabStop = true;
            this.rdbAccountDisable.Text = "Disable";
            this.rdbAccountDisable.UseVisualStyleBackColor = true;
            // 
            // rdbAccountEnable
            // 
            this.rdbAccountEnable.AutoSize = true;
            this.rdbAccountEnable.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.rdbAccountEnable.Location = new System.Drawing.Point(11, 6);
            this.rdbAccountEnable.Name = "rdbAccountEnable";
            this.rdbAccountEnable.Size = new System.Drawing.Size(61, 19);
            this.rdbAccountEnable.TabIndex = 44;
            this.rdbAccountEnable.TabStop = true;
            this.rdbAccountEnable.Text = "Enable";
            this.rdbAccountEnable.UseVisualStyleBackColor = true;
            // 
            // btnCreateAccount
            // 
            this.btnCreateAccount.BackColor = System.Drawing.Color.LimeGreen;
            this.btnCreateAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateAccount.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnCreateAccount.Location = new System.Drawing.Point(258, 470);
            this.btnCreateAccount.Name = "btnCreateAccount";
            this.btnCreateAccount.Size = new System.Drawing.Size(138, 31);
            this.btnCreateAccount.TabIndex = 14;
            this.btnCreateAccount.Text = "Create Account";
            this.btnCreateAccount.UseVisualStyleBackColor = false;
            this.btnCreateAccount.Click += new System.EventHandler(this.btnCreateAccount_Click);
            // 
            // btnUpdateAccount
            // 
            this.btnUpdateAccount.BackColor = System.Drawing.Color.Yellow;
            this.btnUpdateAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateAccount.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpdateAccount.Location = new System.Drawing.Point(402, 470);
            this.btnUpdateAccount.Name = "btnUpdateAccount";
            this.btnUpdateAccount.Size = new System.Drawing.Size(138, 31);
            this.btnUpdateAccount.TabIndex = 14;
            this.btnUpdateAccount.Text = "Update";
            this.btnUpdateAccount.UseVisualStyleBackColor = false;
            this.btnUpdateAccount.Click += new System.EventHandler(this.button7_Click);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(283, 211);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 18);
            this.label15.TabIndex = 43;
            this.label15.Text = "Account Status";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(287, 147);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(253, 20);
            this.txtPassword.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(287, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(216, 18);
            this.label14.TabIndex = 42;
            this.label14.Text = "Password";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(287, 102);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(253, 20);
            this.txtUserName.TabIndex = 39;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(287, 81);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(216, 18);
            this.label13.TabIndex = 40;
            this.label13.Text = "User Name";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.FormattingEnabled = true;
            this.cmbAccountType.Location = new System.Drawing.Point(287, 50);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(253, 21);
            this.cmbAccountType.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(287, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(207, 18);
            this.label12.TabIndex = 37;
            this.label12.Text = "Account Type";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(10, 380);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(207, 18);
            this.label11.TabIndex = 36;
            this.label11.Text = "Birth Date";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(9, 347);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(237, 20);
            this.txtAddress.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(8, 326);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(207, 18);
            this.label10.TabIndex = 34;
            this.label10.Text = "Address";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(8, 296);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 18);
            this.label9.TabIndex = 30;
            this.label9.Text = "Gender";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(9, 264);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(237, 20);
            this.txtLastName.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(8, 243);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(207, 18);
            this.label7.TabIndex = 29;
            this.label7.Text = "Last Name";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.Location = new System.Drawing.Point(8, 220);
            this.txtMiddleName.Name = "txtMiddleName";
            this.txtMiddleName.Size = new System.Drawing.Size(237, 20);
            this.txtMiddleName.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(8, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(207, 18);
            this.label5.TabIndex = 27;
            this.label5.Text = "Middle Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(9, 174);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(237, 20);
            this.txtFirstName.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(8, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 18);
            this.label2.TabIndex = 25;
            this.label2.Text = "First Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pctrEmployee
            // 
            this.pctrEmployee.Image = global::Point_of_Sales_System.Properties.Resources.user;
            this.pctrEmployee.Location = new System.Drawing.Point(9, 66);
            this.pctrEmployee.Name = "pctrEmployee";
            this.pctrEmployee.Size = new System.Drawing.Size(95, 74);
            this.pctrEmployee.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pctrEmployee.TabIndex = 23;
            this.pctrEmployee.TabStop = false;
            // 
            // lblAddSomething
            // 
            this.lblAddSomething.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddSomething.Location = new System.Drawing.Point(4, 4);
            this.lblAddSomething.Name = "lblAddSomething";
            this.lblAddSomething.Size = new System.Drawing.Size(305, 50);
            this.lblAddSomething.TabIndex = 22;
            this.lblAddSomething.Text = "Please Select an account or click add new user account";
            this.lblAddSomething.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Point_of_Sales_System.Properties.Resources.store;
            this.pictureBox2.Location = new System.Drawing.Point(241, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 76);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // tmrRefreshData
            // 
            this.tmrRefreshData.Tick += new System.EventHandler(this.tmrRefreshData_Tick);
            // 
            // AdminDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 609);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AdminDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminDashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminDashboard_FormClosing);
            this.Load += new System.EventHandler(this.AdminDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlReport.ResumeLayout(false);
            this.pnlAddUpdDelProductOption.ResumeLayout(false);
            this.pnlAddUpdDelProductMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.pnlAddUpdOption.ResumeLayout(false);
            this.pnlAddUpdMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.pnlTransactionOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatedCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeftTable)).EndInit();
            this.panel3.ResumeLayout(false);
            this.pnlRightTableOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRightTable)).EndInit();
            this.pnlAddUpdDeleteProduct.ResumeLayout(false);
            this.pnlAddUpdDeleteProduct.PerformLayout();
            this.pnlAddSomething.ResumeLayout(false);
            this.pnlAddSomething.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private PictureBox pictureBox1;
        private Label label1;
        private Button btnViewTransaction;
        private Label lblAdminName;
        private Button button2;
        private Button button1;
        private Panel panel2;
        private Button button4;
        private Button btnUpdateInventory;
        private TextBox txtCashierSearch;
        private Label lblSearchCashier;
        private Label lblTotalSales;
        private Label label3;
        private DataGridView dgvLeftTable;
        private Label lblLeftTable;
        private Panel panel3;
        private PictureBox pictureBox2;
        private Panel pnlRightTableOptions;
        private Label lblTotalPrice;
        private Label lblRightTotalPrice;
        private DataGridView dgvRightTable;
        private Label lblRightTable;
        private ComboBox cmbShowOption;
        private Label label8;
        private System.Windows.Forms.Timer tmrRefreshData;
        private DataGridView dgvLocalCopy;
        private DataGridView dgvUpdatedCopy;
        private Panel pnlTransactionOption;
        private Panel pnlReport;
        private Panel pnlAddSomething;
        private Label label11;
        private TextBox txtAddress;
        private Label label10;
        private RadioButton rdbFemale;
        private RadioButton rdbMale;
        private Label label9;
        private TextBox txtLastName;
        private Label label7;
        private TextBox txtMiddleName;
        private Label label5;
        private TextBox txtFirstName;
        private Label label2;
        private PictureBox pctrEmployee;
        private Label lblAddSomething;
        private RadioButton rdbAccountDisable;
        private RadioButton rdbAccountEnable;
        private Label label15;
        private TextBox txtPassword;
        private Label label14;
        private TextBox txtUserName;
        private Label label13;
        private ComboBox cmbAccountType;
        private Label label12;
        private Button btnCreateAccount;
        private Button btnUpdateAccount;
        private Panel pnlAddUpdOption;
        private Button btnAddNewAccount;
        private Button btnDeleteAccount;
        private Panel pnlAddUpdMessage;
        private Label lblAddUpdText;
        private PictureBox pictureBox4;
        private Panel panel5;
        private Panel panel4;
        private Label lblLoginCount;
        private Button btnViewEmployeeDetails;
        private TextBox txtAccountReason;
        private Label lblAccountReason;
        private Panel pnlAddUpdDelProductOption;
        private Panel pnlAddUpdDelProductMessage;
        private Label lblProductStatus;
        private PictureBox pictureBox3;
        private Button btnAddNewProduct;
        private Button btnDeleteProduct;
        private Panel pnlAddUpdDeleteProduct;
        private TextBox txtProductDescription;
        private Label label6;
        private Button btnCreateProduct;
        private Button btnUpdateProduct;
        private Label label17;
        private TextBox txtStocks;
        private Label label18;
        private Label label19;
        private TextBox txtPrice;
        private Label label20;
        private TextBox txtProductName;
        private Label label25;
        private Label label26;
        private ComboBox cmbProductType;
        private Button btnAddNewCategory;
        private ComboBox cmbDate;
        private ComboBox cmbMonth;
        private ComboBox cmbBirthYear;
        private ComboBox cmbProductDate;
        private ComboBox cmbProductMonth;
        private ComboBox cmbProductYear;
        private Button btnViewInventoryChanges;
        private Button btnViewAccountUpdate;
        private Label label16;
        private TextBox txtProductReason;
        private CheckBox chcbxShowPassword;
    }
}
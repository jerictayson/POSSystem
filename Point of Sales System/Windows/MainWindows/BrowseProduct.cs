﻿using System.Data;
using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.Pop_ups;

namespace Point_of_Sales_System.Windows.MainWindows;

public partial class BrowseProduct : Form
{
    private static Database? _db;
    public static List<Product> customerProducts = new();
    private double _price, _originalPrice, _totalPrice;
    private int _quantity = 1, _maxquantity = 1, _productID;
    private Product _selectedProduct = new();
    private DataTable cmbData = new();
    private Product info;
    public int sessionTime;

    public BrowseProduct(Database? db)
    {
        InitializeComponent();
        _db = db;
    }

    public BrowseProduct()
    {
        InitializeComponent();
    }


    private void btnCloseWindow_Click(object sender, EventArgs e)
    {
        _closeWindow();

        Dispose();
    }

    private void _closeWindow()
    {
        reset();
        new Login().ExitApplication();
    }

    private void CheckCart()
    {
        if (customerProducts.Count == 0)
        {
            btnCheckout.Enabled = false;
            pnlCartMessage.Visible = true;
        }
        else
        {
            btnCheckout.Enabled = true;
            pnlCartMessage.Visible = false;
        }
    }

    private void BrowseProduct_Load(object sender, EventArgs e)
    {
        InitData();
        tmrRefreshData.Start();
        tmrSession.Start();
    }

    private void RefreshValues()
    {
        _db.Query =
            @"SELECT ProductID, [Product Name], Stocks, Price FROM Product WHERE Stocks>0 AND [Expiration Date]>GETDATE();";
        dgvProductTable.DataSource = _db.getTableData("Product").Tables[0];
        dgvOriginalCopy.DataSource = dgvProductTable.DataSource;

        cmbCategory.DisplayMember = "CategoryName";
        cmbCategory.ValueMember = "CategoryID";
        _db.Query = @"SELECT CategoryID AS 'CategoryID', [Category Name] AS 'CategoryName' FROM Category;";
        cmbData = _db.getTableData("Category").Tables[0];

        var NoneSelected = cmbData.NewRow();
        NoneSelected["CategoryName"] = "";
        cmbData.Rows.InsertAt(NoneSelected, 0);
        var defaultSelected = cmbData.NewRow();
        defaultSelected["CategoryName"] = "All";
        cmbData.Rows.InsertAt(defaultSelected, 1);
        cmbCategory.DisplayMember = "CategoryName";
        cmbCategory.ValueMember = "CategoryID";
        cmbCategory.DataSource = cmbData;
        dgvProductTable.ClearSelection();
    }

    private void InitData()
    {
        dgvCartList.ColumnCount = 4;
        RefreshValues();
        lblQuantity.Text = "x";
        txtQuantity.Text = "";
        lblPrice.Text = "";
        lblProductName.Text = "";
        CheckCart();
    }

    private void dgvProductTable_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        txtQuantity.Text = "1";
        info = new Product();
        try
        {
            _price = 0;
            ClearAllValues();
            var productName = dgvProductTable.Rows[e.RowIndex].Cells[0].Value + "";
            _db.Query = $@"SELECT * FROM Product WHERE ProductID={productName}";
            info = _db.getProductRecord();

            lblProductName.Text = info.ProductName;
            _price = info.Price;
            _originalPrice = _price;
            lblPrice.Text = $"₱{_price}";
            _maxquantity = info.Stocks;
            lblDescription.Text = info.Description;
            _quantity = 1;
            lblQuantity.Text = "x";
            _productID = Convert.ToInt32(dgvProductTable.Rows[e.RowIndex].Cells[0].Value + "");
            SetSectedItem();
            btnAddToCart.Enabled = true;
            txtQuantity.Enabled = true;
        }
        catch (Exception ex)
        {
        }
    }

    private void SetSectedItem()
    {
        _selectedProduct = new Product();
        _selectedProduct.Id = _productID;
        _selectedProduct.ProductName = lblProductName.Text.Trim();
        _selectedProduct.Price = Convert.ToInt32(_price);
        _selectedProduct.Quantity = _quantity;
    }

    private void ClearAllValues()
    {
        lblPrice.Text = "";
        lblProductName.Text = "";
        lblQuantity.Text = "";
        _price = 0;
        _maxquantity = 0;
        pnlMessage.Hide();
    }

    private void label6_Click(object sender, EventArgs e)
    {
    }

    private void lblDecrement_Click(object sender, EventArgs e)
    {
        _price = _originalPrice;
        _quantity--;
        if (_quantity >= 1)
            lblQuantity.Text = "x" + _quantity;
        else
            _quantity = 1;

        _price = _price * _quantity;
        lblPrice.Text = $"₱{_price}";
        SetSectedItem();
        pnlMessage.Hide();
    }

    private void lblIncrement_Click(object sender, EventArgs e)
    {
        _price = _originalPrice;
        _quantity++;
        if (_quantity <= _maxquantity)
        {
            lblQuantity.Text = "x" + _quantity;
        }
        else
        {
            _quantity = _maxquantity;
            lblQuantity.Text = "x" + _quantity;
        }

        _price = _price * _quantity;
        lblPrice.Text = $"₱{_price}";

        SetSectedItem();
        pnlMessage.Hide();
    }

    private void BrowseProduct_FormClosing(object sender, FormClosingEventArgs e)
    {
        _closeWindow();
    }

    private void txtSearchBox_KeyUp(object sender, KeyEventArgs e)
    {
        ClearAllValues();
        var category = cmbCategory.Text.Trim();

        if (!string.IsNullOrEmpty(txtSearchBox.Text.Trim()))
        {
            _db.Query =
                $@"SELECT ProductID, [Product Name], Stocks, Price FROM Product WHERE [Product Name] LIKE '%{txtSearchBox.Text}%'";

            if (cmbCategory.SelectedIndex != 0 && cmbCategory.SelectedIndex != 1)
                _db.Query =
                    $@"SELECT ProductID, [Product Name], Stocks, Price FROM Product WHERE [Product Name] LIKE '%{txtSearchBox.Text}%'
                            AND CategoryID=(SELECT CategoryID FROM Category WHERE Category.[Category Name]='{category}')";
            dgvProductTable.DataSource = _db.getTableData("Product").Tables[0];
        }
        else
        {
            _db.Query = @"SELECT ProductID, [Product Name], Stocks, Price FROM Product";
            if (cmbCategory.SelectedIndex != 0 && cmbCategory.SelectedIndex != 1)
                _db.Query =
                    $@"SELECT ProductID, [Product Name], Stocks, Price FROM Product WHERE CategoryID=(SELECT CategoryID FROM Category WHERE Category.[Category Name]='{category}')";
            dgvProductTable.DataSource = _db.getTableData("Product").Tables[0];
        }
    }

    private void button3_Click(object sender, EventArgs e)
    {
        sessionTime = 0;
        var res = MessageBox.Show("Are you sure to proceed your order?", "Confirmation",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        if (res == DialogResult.Yes)
        {
            sessionTime = 0;
            var orderWindow = new frmOrderWindow();
            if (frmOrderWindow._isSuccess)
            {
                orderWindow.Show();
                Hide();
            }
            else
            {
                frmOrderWindow._isSuccess = true;
            }
        }
    }

    private void dgvProductTable_MouseLeave(object sender, EventArgs e)
    {
    }

    private void tmrRefreshData_Tick(object sender, EventArgs e)
    {
        CompareTableCopy();
    }

    private void CompareTableCopy()
    {
        dgvCompareTable.DataSource = null;
        _db.Query =
            @"SELECT ProductID, [Product Name], Stocks, Price FROM Product WHERE Stocks>0 AND [Expiration Date]>GETDATE()";
        dgvCompareTable.DataSource = _db.getTableData("Product").Tables[0];
        _db.Query = @"SELECT [Category Name] AS 'Category' FROM Category;";
        var newValues = _db.getTableData("Category").Tables[0];
        var lastIndex = cmbCategory.SelectedIndex;
        if (newValues.Rows.Count != cmbData.Rows.Count - 2)
        {
            RefreshValues();
            cmbCategory.SelectedIndex = lastIndex;
        }
        else
        {
            for (var i = 0; i < cmbData.Rows.Count; i++)
            {
                if (i == 0 || i == 1)
                    continue;
                for (var j = 0; j < newValues.Rows.Count; j++)
                    if (newValues.Rows[j].Equals(cmbData.Rows[i]))
                    {
                        RefreshValues();
                        cmbCategory.SelectedIndex = lastIndex;
                        break;
                    }
            }

            if (dgvOriginalCopy.RowCount == dgvCompareTable.RowCount)
            {
                for (var i = 0; i < dgvOriginalCopy.RowCount; i++)
                for (var j = 0; j < dgvOriginalCopy.ColumnCount; j++)
                    if (dgvOriginalCopy.Rows[i].Cells[j].Value + "" != dgvCompareTable.Rows[i].Cells[j].Value + "")
                    {
                        RefreshValues();
                        cmbCategory.SelectedIndex = lastIndex;
                        break;
                    }
            }
            else
            {
                RefreshValues();
                cmbCategory.SelectedIndex = lastIndex;
            }
        }
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
        sessionTime = 0;
        var res = MessageBox.Show("Are you sure to remove the selected products to your cart?",
            "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            foreach (DataGridViewRow row in dgvCartList.SelectedRows)
            {
                var productId = Convert.ToInt32(row.Cells[0].Value + "");
                var quantity = Convert.ToInt32(row.Cells[2].Value + "");
                _db.Query = $@"SELECT * From Product WHERE ProductID='{productId}';";
                var info = _db.getProductRecord();
                var currentStock = info.Stocks;
                _db.Query = $@"UPDATE Product SET Stocks={currentStock + quantity} WHERE
                                 ProductID='{productId}'";
                _db.AddUpdateDeleteRecord();
                dgvCartList.Rows.RemoveAt(row.Index);
                for (var i = 0; i < customerProducts.Count; i++)
                    if (customerProducts[i].Id == productId)
                    {
                        customerProducts.RemoveAt(i);
                        break;
                    }
            }

            RefreshCartList();
            CheckCart();
        }
    }

    private void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        sessionTime = 0;
    }

    private void txtQuantity_KeyUp(object sender, KeyEventArgs e)
    {
        sessionTime = 0;
        _quantity = 1;
        _price = _originalPrice;
        try
        {
            if (txtQuantity.Text.Trim().Length != 0)
            {
                _quantity = Convert.ToInt32(txtQuantity.Text.Trim());
                if (_quantity <= info.Stocks)
                {
                    _price = info.Price * _quantity;
                    lblPrice.Text = $"₱{_price}";
                    _selectedProduct.Quantity = _quantity;
                    _selectedProduct.Price = _price;
                    btnAddToCart.Enabled = true;
                }
                else
                {
                    btnAddToCart.Enabled = false;
                }
            }
            else
            {
                _price = info.Price;
                lblPrice.Text = $"₱{_price}";
                btnAddToCart.Enabled = false;
            }
        }
        catch (Exception)
        {
            btnAddToCart.Enabled = false;
        }
    }

    private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        sessionTime = 0;
        if (cmbCategory.SelectedIndex != 0)
        {
            pnlBoxMessage.Visible = false;
            if (cmbCategory.SelectedIndex == 1)
            {
                _db.Query = @"SELECT ProductID, [Product Name], Stocks, Price 
                    FROM Product WHERE Stocks>0 AND [Expiration Date]>GETDATE();";
                dgvProductTable.DataSource = _db.getTableData("Product").Tables[0];
            }
            else
            {
                _db.Query = $@"SELECT ProductID, [Product Name], Stocks, Price 
                    FROM Product WHERE CategoryID='{cmbCategory.SelectedValue + ""}' AND Stocks>0;";
                dgvProductTable.DataSource = _db.getTableData("Product").Tables[0];
            }
        }
        else
        {
            pnlBoxMessage.Visible = true;
        }
    }

    private void dgvCartList_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        btnDelete.Enabled = true;
    }

    private void txtSearchBox_TextChanged(object sender, EventArgs e)
    {
        sessionTime = 0;
    }

    private void button2_Click(object sender, EventArgs e)
    {
        new ViewCart(customerProducts).Show();
        Hide();
    }

    private void dgvProductTable_Click(object sender, EventArgs e)
    {
    }

    private void tmrIDLEcount_Tick(object sender, EventArgs e)
    {
    }

    private void tmrSession_Tick_1(object sender, EventArgs e)
    {
        sessionTime++;
        //label17.Text = sessionTime.ToString();
        if (sessionTime == 300) reset();
    }

    private void button1_Click(object sender, EventArgs e)
    {
        sessionTime = 0;
        var isDuplicate = false;

        _db.Query = $@"SELECT * From Product WHERE ProductID='{_productID}';";
        var info = _db.getProductRecord();

        if (info.Stocks == 0)
        {
            MessageBox.Show("Item is out of stock.");
            txtQuantity.Text = "";
            ClearAllValues();
        }
        else if (info.Stocks < _quantity)
        {
            MessageBox.Show("Please enter the quantity again.");
            txtQuantity.Text = "";
        }
        else
        {
            foreach (var product in customerProducts)
                if (product.Id == _selectedProduct.Id)
                {
                    product.Quantity += _selectedProduct.Quantity;
                    product.Price = product.Price + _price;
                    isDuplicate = true;
                }

            if (!isDuplicate)
                customerProducts.Add(_selectedProduct);
            _db.Query = $@"SELECT * FROM Product WHERE ProductID='{_selectedProduct.Id}'";
            var productRecord = _db.getProductRecord();
            _db.Query = $@"UPDATE Product SET Stocks={productRecord.Stocks - _selectedProduct.Quantity}
                        WHERE ProductID='{_selectedProduct.Id}'";
            _db.AddUpdateDeleteRecord();
            CheckCart();
            pnlMessage.Show();
            RefreshCartList();
            _selectedProduct = null;
            btnAddToCart.Enabled = false;
        }
    }

    private void RefreshCartList()
    {
        _totalPrice = 0;
        dgvCartList.Rows.Clear();
        if (customerProducts.Count != 0)
            foreach (var product in customerProducts)
            {
                dgvCartList.Rows.Add(product.Id, product.ProductName, product.Quantity, product.Price);
                _totalPrice += product.Price;
            }

        dgvCartList.ClearSelection();

        txtPrice.Text = $"₱{_totalPrice.ToString("#.##")}";
    }

    private void tmrSession_Tick(object sender, EventArgs e)
    {
    }

    public void reset()
    {
        for (var i = 0; i < customerProducts.Count; i++)
        {
            _db.Query = $@"UPDATE Product SET Stocks=((SELECT Stocks FROM Product 
                WHERE ProductID='{customerProducts[i].Id}') + {customerProducts[i].Quantity})
                            WHERE ProductID='{customerProducts[i].Id}'";
            _db.AddUpdateDeleteRecord();
            dgvProductTable.DataSource = null;
            pnlBoxMessage.Hide();
            pnlBoxMessage.Visible = true;
            btnCheckout.Enabled = false;
            btnAddToCart.Enabled = true;
            txtQuantity.Enabled = false;
            pnlCartMessage.Visible = true;
            cmbCategory.SelectedIndex = 0;
            txtPrice.Text = "Total Price";
            txtQuantity.Text = "";
            lblPrice.Text = "";
            lblProductName.Text = "";
            txtQuantity.Text = "";
            lblDescription.Text = "";
            dgvCartList.Rows.RemoveAt(i);
            customerProducts.RemoveAt(i);
            pnlMessage.Hide();
        }
    }
}
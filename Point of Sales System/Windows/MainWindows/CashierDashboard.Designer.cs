﻿using System.Windows.Forms;

namespace Point_of_Sales_System.Windows.MainWindows
{
    partial class CashierDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlLeftTableMessage = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.txtSearchOrder = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlFilterOrders = new System.Windows.Forms.Panel();
            this.cmbFilterOrders = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvUpdatedCopy = new System.Windows.Forms.DataGridView();
            this.dgvLocalCopy = new System.Windows.Forms.DataGridView();
            this.dgvLeftTable = new System.Windows.Forms.DataGridView();
            this.lblFirstPanel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlViewHistory = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnViewInventory = new System.Windows.Forms.Button();
            this.btnViewTransactionHistory = new System.Windows.Forms.Button();
            this.btnOrders = new System.Windows.Forms.Button();
            this.pctrbxCashier = new System.Windows.Forms.PictureBox();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.pctrBoxLeftTable = new System.Windows.Forms.PictureBox();
            this.pnlTransactionButtons = new System.Windows.Forms.Panel();
            this.btnRejectOrder = new System.Windows.Forms.Button();
            this.btnProcessOrder = new System.Windows.Forms.Button();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.pnlMoneyBox = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomerMoney = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblExchange = new System.Windows.Forms.Label();
            this.lblTotalAmountLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvRightTable = new System.Windows.Forms.DataGridView();
            this.lblSecondPanelTitle = new System.Windows.Forms.Label();
            this.tmrRefreshData = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.pnlLeftTableMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlSearch.SuspendLayout();
            this.pnlFilterOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatedCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeftTable)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxCashier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxLeftTable)).BeginInit();
            this.pnlTransactionButtons.SuspendLayout();
            this.pnlMoneyBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRightTable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Khaki;
            this.panel1.Controls.Add(this.pnlLeftTableMessage);
            this.panel1.Controls.Add(this.pnlSearch);
            this.panel1.Controls.Add(this.pnlFilterOrders);
            this.panel1.Controls.Add(this.dgvUpdatedCopy);
            this.panel1.Controls.Add(this.dgvLocalCopy);
            this.panel1.Controls.Add(this.dgvLeftTable);
            this.panel1.Controls.Add(this.lblFirstPanel);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 618);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pnlLeftTableMessage
            // 
            this.pnlLeftTableMessage.BackColor = System.Drawing.SystemColors.Control;
            this.pnlLeftTableMessage.Controls.Add(this.label8);
            this.pnlLeftTableMessage.Controls.Add(this.pictureBox2);
            this.pnlLeftTableMessage.Location = new System.Drawing.Point(182, 112);
            this.pnlLeftTableMessage.Name = "pnlLeftTableMessage";
            this.pnlLeftTableMessage.Size = new System.Drawing.Size(544, 467);
            this.pnlLeftTableMessage.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(23, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(487, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "The list is Empty";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Point_of_Sales_System.Properties.Resources.no_task;
            this.pictureBox2.Location = new System.Drawing.Point(203, 105);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(129, 136);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.txtSearchOrder);
            this.pnlSearch.Controls.Add(this.label7);
            this.pnlSearch.Location = new System.Drawing.Point(188, 64);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(269, 42);
            this.pnlSearch.TabIndex = 8;
            // 
            // txtSearchOrder
            // 
            this.txtSearchOrder.Location = new System.Drawing.Point(118, 9);
            this.txtSearchOrder.Name = "txtSearchOrder";
            this.txtSearchOrder.Size = new System.Drawing.Size(135, 20);
            this.txtSearchOrder.TabIndex = 7;
            this.txtSearchOrder.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchOrder_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 21);
            this.label7.TabIndex = 6;
            this.label7.Text = "Search Order";
            // 
            // pnlFilterOrders
            // 
            this.pnlFilterOrders.Controls.Add(this.cmbFilterOrders);
            this.pnlFilterOrders.Controls.Add(this.label2);
            this.pnlFilterOrders.Location = new System.Drawing.Point(538, 64);
            this.pnlFilterOrders.Name = "pnlFilterOrders";
            this.pnlFilterOrders.Size = new System.Drawing.Size(183, 42);
            this.pnlFilterOrders.TabIndex = 7;
            this.pnlFilterOrders.Visible = false;
            // 
            // cmbFilterOrders
            // 
            this.cmbFilterOrders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilterOrders.FormattingEnabled = true;
            this.cmbFilterOrders.Location = new System.Drawing.Point(59, 11);
            this.cmbFilterOrders.Name = "cmbFilterOrders";
            this.cmbFilterOrders.Size = new System.Drawing.Size(121, 21);
            this.cmbFilterOrders.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "Show";
            // 
            // dgvUpdatedCopy
            // 
            this.dgvUpdatedCopy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdatedCopy.Location = new System.Drawing.Point(704, 41);
            this.dgvUpdatedCopy.Name = "dgvUpdatedCopy";
            this.dgvUpdatedCopy.Size = new System.Drawing.Size(17, 14);
            this.dgvUpdatedCopy.TabIndex = 5;
            this.dgvUpdatedCopy.Visible = false;
            // 
            // dgvLocalCopy
            // 
            this.dgvLocalCopy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocalCopy.Location = new System.Drawing.Point(681, 41);
            this.dgvLocalCopy.Name = "dgvLocalCopy";
            this.dgvLocalCopy.Size = new System.Drawing.Size(17, 14);
            this.dgvLocalCopy.TabIndex = 4;
            this.dgvLocalCopy.Visible = false;
            // 
            // dgvLeftTable
            // 
            this.dgvLeftTable.AllowUserToAddRows = false;
            this.dgvLeftTable.AllowUserToDeleteRows = false;
            this.dgvLeftTable.AllowUserToResizeColumns = false;
            this.dgvLeftTable.AllowUserToResizeRows = false;
            this.dgvLeftTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLeftTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvLeftTable.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvLeftTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLeftTable.Location = new System.Drawing.Point(185, 112);
            this.dgvLeftTable.Name = "dgvLeftTable";
            this.dgvLeftTable.ReadOnly = true;
            this.dgvLeftTable.RowHeadersVisible = false;
            this.dgvLeftTable.RowTemplate.Height = 25;
            this.dgvLeftTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLeftTable.Size = new System.Drawing.Size(536, 467);
            this.dgvLeftTable.TabIndex = 0;
            this.dgvLeftTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderList_CellClick);
            // 
            // lblFirstPanel
            // 
            this.lblFirstPanel.AutoSize = true;
            this.lblFirstPanel.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblFirstPanel.Location = new System.Drawing.Point(180, 39);
            this.lblFirstPanel.Name = "lblFirstPanel";
            this.lblFirstPanel.Size = new System.Drawing.Size(74, 25);
            this.lblFirstPanel.TabIndex = 2;
            this.lblFirstPanel.Text = "Orders:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Khaki;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pnlViewHistory);
            this.panel2.Controls.Add(this.btnLogout);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnViewInventory);
            this.panel2.Controls.Add(this.btnViewTransactionHistory);
            this.panel2.Controls.Add(this.btnOrders);
            this.panel2.Controls.Add(this.pctrbxCashier);
            this.panel2.Controls.Add(this.lblEmployeeName);
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(178, 618);
            this.panel2.TabIndex = 0;
            // 
            // pnlViewHistory
            // 
            this.pnlViewHistory.Location = new System.Drawing.Point(183, -1);
            this.pnlViewHistory.Name = "pnlViewHistory";
            this.pnlViewHistory.Size = new System.Drawing.Size(1086, 618);
            this.pnlViewHistory.TabIndex = 10;
            this.pnlViewHistory.Visible = false;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.MediumAquamarine;
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepPink;
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnLogout.Location = new System.Drawing.Point(-1, 575);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(1);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(177, 41);
            this.btnLogout.TabIndex = 9;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "Welcome!";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnViewInventory
            // 
            this.btnViewInventory.BackColor = System.Drawing.Color.MediumAquamarine;
            this.btnViewInventory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewInventory.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepPink;
            this.btnViewInventory.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnViewInventory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewInventory.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewInventory.Location = new System.Drawing.Point(0, 263);
            this.btnViewInventory.Margin = new System.Windows.Forms.Padding(1);
            this.btnViewInventory.Name = "btnViewInventory";
            this.btnViewInventory.Size = new System.Drawing.Size(176, 48);
            this.btnViewInventory.TabIndex = 7;
            this.btnViewInventory.Text = "View Inventory";
            this.btnViewInventory.UseVisualStyleBackColor = false;
            this.btnViewInventory.Click += new System.EventHandler(this.btnViewInventory_Click);
            // 
            // btnViewTransactionHistory
            // 
            this.btnViewTransactionHistory.BackColor = System.Drawing.Color.MediumAquamarine;
            this.btnViewTransactionHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewTransactionHistory.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepPink;
            this.btnViewTransactionHistory.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnViewTransactionHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewTransactionHistory.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnViewTransactionHistory.Location = new System.Drawing.Point(-2, 216);
            this.btnViewTransactionHistory.Margin = new System.Windows.Forms.Padding(1);
            this.btnViewTransactionHistory.Name = "btnViewTransactionHistory";
            this.btnViewTransactionHistory.Size = new System.Drawing.Size(181, 45);
            this.btnViewTransactionHistory.TabIndex = 6;
            this.btnViewTransactionHistory.Text = "Transaction History";
            this.btnViewTransactionHistory.UseVisualStyleBackColor = false;
            this.btnViewTransactionHistory.Click += new System.EventHandler(this.btnViewTransactionHistory_Click);
            // 
            // btnOrders
            // 
            this.btnOrders.BackColor = System.Drawing.Color.DeepPink;
            this.btnOrders.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOrders.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepPink;
            this.btnOrders.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.btnOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrders.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnOrders.Location = new System.Drawing.Point(-2, 173);
            this.btnOrders.Margin = new System.Windows.Forms.Padding(1);
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Size = new System.Drawing.Size(179, 41);
            this.btnOrders.TabIndex = 5;
            this.btnOrders.Text = "Orders";
            this.btnOrders.UseVisualStyleBackColor = false;
            this.btnOrders.Click += new System.EventHandler(this.btnOrders_Click);
            // 
            // pctrbxCashier
            // 
            this.pctrbxCashier.Image = global::Point_of_Sales_System.Properties.Resources.user;
            this.pctrbxCashier.Location = new System.Drawing.Point(-2, 45);
            this.pctrbxCashier.Name = "pctrbxCashier";
            this.pctrbxCashier.Size = new System.Drawing.Size(179, 72);
            this.pctrbxCashier.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pctrbxCashier.TabIndex = 4;
            this.pctrbxCashier.TabStop = false;
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.lblEmployeeName.Location = new System.Drawing.Point(3, 134);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(174, 22);
            this.lblEmployeeName.TabIndex = 3;
            this.lblEmployeeName.Text = "Hello";
            this.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Point_of_Sales_System.Properties.Resources.store;
            this.pictureBox1.Location = new System.Drawing.Point(216, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.LightGreen;
            this.panel4.Controls.Add(this.btnDeleteItem);
            this.panel4.Controls.Add(this.pctrBoxLeftTable);
            this.panel4.Controls.Add(this.pnlTransactionButtons);
            this.panel4.Controls.Add(this.lblTotalPrice);
            this.panel4.Controls.Add(this.pnlMoneyBox);
            this.panel4.Controls.Add(this.lblTotalAmountLabel);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.dgvRightTable);
            this.panel4.Controls.Add(this.lblSecondPanelTitle);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Location = new System.Drawing.Point(726, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(547, 619);
            this.panel4.TabIndex = 1;
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Enabled = false;
            this.btnDeleteItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteItem.Image = global::Point_of_Sales_System.Properties.Resources.trash_bin;
            this.btnDeleteItem.Location = new System.Drawing.Point(449, 73);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(81, 42);
            this.btnDeleteItem.TabIndex = 18;
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // pctrBoxLeftTable
            // 
            this.pctrBoxLeftTable.Image = global::Point_of_Sales_System.Properties.Resources.list;
            this.pctrBoxLeftTable.Location = new System.Drawing.Point(13, 119);
            this.pctrBoxLeftTable.Name = "pctrBoxLeftTable";
            this.pctrBoxLeftTable.Size = new System.Drawing.Size(517, 285);
            this.pctrBoxLeftTable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pctrBoxLeftTable.TabIndex = 17;
            this.pctrBoxLeftTable.TabStop = false;
            // 
            // pnlTransactionButtons
            // 
            this.pnlTransactionButtons.Controls.Add(this.btnRejectOrder);
            this.pnlTransactionButtons.Controls.Add(this.btnProcessOrder);
            this.pnlTransactionButtons.Location = new System.Drawing.Point(223, 543);
            this.pnlTransactionButtons.Name = "pnlTransactionButtons";
            this.pnlTransactionButtons.Size = new System.Drawing.Size(310, 64);
            this.pnlTransactionButtons.TabIndex = 16;
            // 
            // btnRejectOrder
            // 
            this.btnRejectOrder.BackColor = System.Drawing.Color.Red;
            this.btnRejectOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRejectOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRejectOrder.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnRejectOrder.Location = new System.Drawing.Point(163, 14);
            this.btnRejectOrder.Name = "btnRejectOrder";
            this.btnRejectOrder.Size = new System.Drawing.Size(144, 42);
            this.btnRejectOrder.TabIndex = 14;
            this.btnRejectOrder.Text = "Reject Order";
            this.btnRejectOrder.UseVisualStyleBackColor = false;
            this.btnRejectOrder.Click += new System.EventHandler(this.btnRejectOrder_Click);
            // 
            // btnProcessOrder
            // 
            this.btnProcessOrder.BackColor = System.Drawing.Color.ForestGreen;
            this.btnProcessOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcessOrder.Enabled = false;
            this.btnProcessOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessOrder.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnProcessOrder.Location = new System.Drawing.Point(3, 14);
            this.btnProcessOrder.Name = "btnProcessOrder";
            this.btnProcessOrder.Size = new System.Drawing.Size(154, 43);
            this.btnProcessOrder.TabIndex = 13;
            this.btnProcessOrder.Text = "Process Order";
            this.btnProcessOrder.UseVisualStyleBackColor = false;
            this.btnProcessOrder.Click += new System.EventHandler(this.btnProcessOrder_Click);
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblTotalPrice.Location = new System.Drawing.Point(424, 429);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(47, 25);
            this.lblTotalPrice.TabIndex = 12;
            this.lblTotalPrice.Text = "1.00";
            // 
            // pnlMoneyBox
            // 
            this.pnlMoneyBox.Controls.Add(this.label3);
            this.pnlMoneyBox.Controls.Add(this.txtCustomerMoney);
            this.pnlMoneyBox.Controls.Add(this.label4);
            this.pnlMoneyBox.Controls.Add(this.lblExchange);
            this.pnlMoneyBox.Location = new System.Drawing.Point(13, 412);
            this.pnlMoneyBox.Name = "pnlMoneyBox";
            this.pnlMoneyBox.Size = new System.Drawing.Size(204, 195);
            this.pnlMoneyBox.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(1, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 54);
            this.label3.TabIndex = 6;
            this.label3.Text = "Enter Customer Payment Amount";
            // 
            // txtCustomerMoney
            // 
            this.txtCustomerMoney.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerMoney.Location = new System.Drawing.Point(3, 74);
            this.txtCustomerMoney.Name = "txtCustomerMoney";
            this.txtCustomerMoney.Size = new System.Drawing.Size(193, 29);
            this.txtCustomerMoney.TabIndex = 7;
            this.txtCustomerMoney.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCustomerMoney_KeyUp);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(3, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Exchange:";
            // 
            // lblExchange
            // 
            this.lblExchange.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblExchange.Location = new System.Drawing.Point(3, 145);
            this.lblExchange.Name = "lblExchange";
            this.lblExchange.Size = new System.Drawing.Size(193, 23);
            this.lblExchange.TabIndex = 9;
            this.lblExchange.Text = "1.00";
            // 
            // lblTotalAmountLabel
            // 
            this.lblTotalAmountLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblTotalAmountLabel.Location = new System.Drawing.Point(329, 429);
            this.lblTotalAmountLabel.Name = "lblTotalAmountLabel";
            this.lblTotalAmountLabel.Size = new System.Drawing.Size(89, 48);
            this.lblTotalAmountLabel.TabIndex = 10;
            this.lblTotalAmountLabel.Text = "Total Amount:";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(330, 445);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "_________________________";
            // 
            // dgvRightTable
            // 
            this.dgvRightTable.AllowUserToAddRows = false;
            this.dgvRightTable.AllowUserToDeleteRows = false;
            this.dgvRightTable.AllowUserToResizeRows = false;
            this.dgvRightTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRightTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRightTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRightTable.Location = new System.Drawing.Point(13, 121);
            this.dgvRightTable.Name = "dgvRightTable";
            this.dgvRightTable.ReadOnly = true;
            this.dgvRightTable.RowHeadersVisible = false;
            this.dgvRightTable.RowTemplate.Height = 25;
            this.dgvRightTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRightTable.Size = new System.Drawing.Size(517, 281);
            this.dgvRightTable.TabIndex = 5;
            this.dgvRightTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRightTable_CellClick);
            // 
            // lblSecondPanelTitle
            // 
            this.lblSecondPanelTitle.AutoSize = true;
            this.lblSecondPanelTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblSecondPanelTitle.Location = new System.Drawing.Point(8, 93);
            this.lblSecondPanelTitle.Name = "lblSecondPanelTitle";
            this.lblSecondPanelTitle.Size = new System.Drawing.Size(130, 25);
            this.lblSecondPanelTitle.TabIndex = 4;
            this.lblSecondPanelTitle.Text = "Order Details:";
            // 
            // tmrRefreshData
            // 
            this.tmrRefreshData.Tick += new System.EventHandler(this.tmrRefreshData_Tick);
            // 
            // CashierDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 619);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CashierDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeDashboard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeDashboard_FormClosing);
            this.Load += new System.EventHandler(this.EmployeeDashboard_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlLeftTableMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlFilterOrders.ResumeLayout(false);
            this.pnlFilterOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdatedCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLeftTable)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxCashier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrBoxLeftTable)).EndInit();
            this.pnlTransactionButtons.ResumeLayout(false);
            this.pnlMoneyBox.ResumeLayout(false);
            this.pnlMoneyBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRightTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private PictureBox pictureBox1;
        private Panel panel2;
        private Panel pnlLeftTableMessage;
        private DataGridView dgvLeftTable;
        private Label lblFirstPanel;
        private Panel panel4;
        private Label lblSecondPanelTitle;
        private Label lblTotalAmountLabel;
        private Label lblExchange;
        private Label label4;
        private TextBox txtCustomerMoney;
        private Label label3;
        private DataGridView dgvRightTable;
        private Label label6;
        private Label lblTotalPrice;
        private Button btnRejectOrder;
        private Button btnProcessOrder;
        private Button btnLogout;
        private Label label1;
        private Button btnViewInventory;
        private Button btnViewTransactionHistory;
        private Button btnOrders;
        private PictureBox pctrbxCashier;
        private Label lblEmployeeName;
        private Panel pnlViewHistory;
        private System.Windows.Forms.Timer tmrRefreshData;
        private DataGridView dgvUpdatedCopy;
        private DataGridView dgvLocalCopy;
        private Panel pnlSearch;
        private TextBox txtSearchOrder;
        private Label label7;
        private Panel pnlFilterOrders;
        private ComboBox cmbFilterOrders;
        private Label label2;
        private Panel pnlTransactionButtons;
        private Panel pnlMoneyBox;
        private PictureBox pctrBoxLeftTable;
        private PictureBox pictureBox2;
        private Label label8;
        private Button btnDeleteItem;
    }
}
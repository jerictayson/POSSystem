﻿using System.Data;
using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.Pop_ups;

namespace Point_of_Sales_System.Windows.MainWindows;

public partial class CashierDashboard : Form
{
    private static Employee? _employee;
    private readonly Database? _db = new();
    private string _mode = "", receiptTemplatePath;
    private int _rowIndex = 0, _productID = 0;
    private double _totalPrice, _customerMoney, _customerExchange;
    private DataTable cmbFilterData = new();
    private int ID;

    public CashierDashboard()
    {
        InitializeComponent();
    }

    public CashierDashboard(Employee session)
    {
        InitializeComponent();
        _employee = session;
    }

    private void UpdateLeftTable()
    {
        switch (_mode)
        {
            case "Transaction":


                _db.Query = $@"SELECT DISTINCT Status FROM CustomerOrder WHERE EmployeeID='{_employee.Id}'";
                cmbFilterData = _db.getTableData("CustomerOrder").Tables[0];
                var defaultRow = cmbFilterData.NewRow();
                defaultRow["Status"] = "All";
                cmbFilterData.Rows.InsertAt(defaultRow, 0);
                cmbFilterOrders.DisplayMember = "Status";
                cmbFilterOrders.ValueMember = "Status";

                cmbFilterOrders.DataSource = cmbFilterData;


                _db.Query =
                    $@"SELECT [Order ID], Status,  [Transaction Date] FROM CustomerOrder WHERE EmployeeID='{_employee.Id}' AND Status='Pending'";
                dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];
                break;
            case "TransactionHistory":

                _db.Query =
                    $@"SELECT [Order ID], Status, [Transaction Date] FROM CustomerOrder WHERE EmployeeID={_employee.Id} AND NOT Status='Pending'";
                dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];

                break;
            case "ViewInventory":

                _db.Query = @"SELECT ProductID, [Product Name], Price, Stocks, Description, 
                                [Expiration Date], Category.[Category Name]
                    FROM Product INNER JOIN Category ON Category.CategoryID = Product.CategoryID";
                dgvLeftTable.DataSource = _db.getTableData("Product").Tables[0];
                break;
        }

        dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
    }

    private void EmployeeDashboard_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(_employee.ProfilePicture))
            pctrbxCashier.Image = Image.FromFile(_employee.ProfilePicture);
        lblEmployeeName.Text = $"{_employee.FirstName} {_employee.LastName}";
        _mode = "Transaction";
        UpdateLeftTable();
        tmrRefreshData.Start();
        dgvLeftTable.ClearSelection();
    }

    private bool CloseWindow()
    {
        var res = MessageBox.Show("Are you sure you want to logout?",
            "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            _employee.IsLoggedIn = false;
            _db.Query = $@"UPDATE EmployeeCredential SET LoggedIn={Convert.ToInt32(_employee.IsLoggedIn)} 
                            WHERE [User Name]='{_employee.UserName}'";
            _db.AddUpdateDeleteRecord();
            var login = new EmployeeLogin(_db);
            login.Show();
            Dispose();
        }

        return true;
    }

    private void panel1_Paint(object sender, PaintEventArgs e)
    {
    }

    private void dgvOrderList_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        try
        {
            ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
            ShowRightTableItem();
        }
        catch (Exception)
        {
        }
    }

    private void ShowRightTableItem()
    {
        switch (_mode)
        {
            case "Transaction":
                _totalPrice = 0;
                _customerExchange = 0;
                btnRejectOrder.Enabled = true;
                lblExchange.Text = "0 PHP";
                txtCustomerMoney.Text = "";
                _db.Query = $@"SELECT OrderDetails.ProductID, Product.[Product Name],
                        Product.Price, Quantity, Total FROM OrderDetails INNER JOIN 
                        Product ON Product.ProductID = OrderDetails.ProductID WHERE OrderID='{ID}';";
                dgvRightTable.DataSource = _db.getTableData("OrderDetails").Tables[0];
                foreach (DataGridViewRow row in dgvRightTable.Rows)
                {
                    var price = row.Cells[4].Value.ToString();
                    _totalPrice += Convert.ToDouble(price.Trim());
                }

                lblTotalPrice.Text = $"{_totalPrice.ToString("#.##")} PHP";

                break;
            case "TransactionHistory":

                _totalPrice = 0;
                _db.Query = $@"SELECT OrderDetails.ProductID, Product.[Product Name],
                        Product.Price, Quantity, Total FROM OrderDetails INNER JOIN 
                        Product ON Product.ProductID = OrderDetails.ProductID WHERE OrderID='{ID}';";
                dgvRightTable.DataSource = _db.getTableData("OrderDetails").Tables[0];
                foreach (DataGridViewRow row in dgvRightTable.Rows)
                {
                    var price = row.Cells[4].Value.ToString();
                    _totalPrice += Convert.ToDouble(price.Trim());
                }

                lblTotalPrice.Text = $"{_totalPrice.ToString("#.##")} PHP";

                break;
            case "ViewInventory":

                break;
        }
    }

    //wala tong reference bind mo sya sa click ng btnReject
    private void btnDeleteOrder_Click(object sender, EventArgs e)
    {
    }

    private void EmployeeDashboard_FormClosing(object sender, FormClosingEventArgs e)
    {
        e.Cancel = CloseWindow();
    }

    private void btnOrders_Click(object sender, EventArgs e)
    {
        pnlSearch.Visible = true;
        _mode = "Transaction";
        dgvRightTable.DataSource = null;
        _totalPrice = 0;
        lblTotalPrice.Text = "";
        UpdateLeftTable();
        pnlMoneyBox.Visible = true;
        pnlTransactionButtons.Visible = true;
        btnOrders.BackColor = Color.DeepPink;
        btnViewTransactionHistory.BackColor = Color.MediumAquamarine;
        btnViewInventory.BackColor = Color.MediumAquamarine;
    }

    private void btnViewTransactionHistory_Click(object sender, EventArgs e)
    {
        pnlSearch.Visible = true;
        btnDeleteItem.Enabled = false;
        btnOrders.BackColor = Color.MediumAquamarine;
        lblTotalAmountLabel.Visible = true;
        pnlMoneyBox.Visible = false;
        pnlTransactionButtons.Visible = false;
        btnViewTransactionHistory.BackColor = Color.DeepPink;
        btnViewInventory.BackColor = Color.MediumAquamarine;
        dgvRightTable.DataSource = null;
        _mode = "TransactionHistory";
        UpdateLeftTable();
    }

    private void btnViewInventory_Click(object sender, EventArgs e)
    {
        pnlSearch.Visible = false;
        btnDeleteItem.Enabled = false;
        btnOrders.BackColor = Color.MediumAquamarine;
        lblTotalPrice.Text = "";
        lblTotalAmountLabel.Visible = false;
        btnViewTransactionHistory.BackColor = Color.MediumAquamarine;
        btnViewInventory.BackColor = Color.DeepPink;
        pnlTransactionButtons.Visible = false;
        pnlMoneyBox.Visible = false;
        dgvRightTable.DataSource = null;
        _mode = "ViewInventory";
        UpdateLeftTable();
    }

    private void btnProcessOrder_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Are you sure you want to confirm this order?", "Confirmation",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            _db.Query = $@"UPDATE CustomerOrder SET Status='Accepted' WHERE [Order ID]='{ID}'";
            _db.AddUpdateDeleteRecord();
            _db.Query = $@"UPDATE CustomerOrder SET Exchange='{_customerExchange}' WHERE [Order ID]='{ID}'";
            _db.AddUpdateDeleteRecord();
            _db.Query = $@"UPDATE CustomerOrder SET [Total Price]='{_totalPrice}' WHERE [Order ID]='{ID}'";
            _db.AddUpdateDeleteRecord();
            _db.Query = $@"INSERT INTO TransactionHistory VALUES ('{_employee.Id}','{ID}')";
            _db.AddUpdateDeleteRecord();
            MessageBox.Show("Success!");

            _db.Query =
                $@"SELECT OrderID, CustomerOrder.Exchange, CONCAT(Employee.[First Name],' ', Employee.[Last Name]) AS 'Cashier Name', Product.[Product Name], Quantity, Total  FROM OrderDetails INNER JOIN
                            Product ON OrderDetails.ProductID = Product.ProductID INNER JOIN CustomerOrder ON
							OrderDetails.OrderID = CustomerOrder.[Order ID]
							INNER JOIN Employee ON CustomerOrder.EmployeeID = Employee.EmployeeID
							WHERE OrderDetails.OrderID='{ID}'";

            Receipt.DataSet1 = _db.getTableData("OrderDetails");
            _db.WriteXml(Application.StartupPath + @"\receipt.xml");
            receiptTemplatePath = Directory
                .GetParent(Directory.GetParent(Directory.GetParent(Application.StartupPath).FullName).FullName)
                .FullName;
            Receipt.XMLPath = $@"{receiptTemplatePath}\ReceiptStatement.rpt";
            var window = new Receipt();
            window.ShowDialog();
            ClearInput();
            dgvRightTable.DataSource = null;
            dgvLeftTable.ClearSelection();
        }
    }

    private void btnRejectOrder_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Do you want to delete this order?",
            "Confirmation",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            foreach (DataGridViewRow row in dgvRightTable.Rows)
            {
                var id = Convert.ToInt32(row.Cells[0].Value + "");
                var quantity = Convert.ToInt32(row.Cells[3].Value + "");
                _db.Query = $@"UPDATE Product SET Stocks=((SELECT Stocks FROM Product 
                    WHERE ProductID='{id}') + {quantity}) WHERE ProductID='{id}'";
                _db.AddUpdateDeleteRecord();
            }

            _db.Query = $@"UPDATE CustomerOrder SET Status='Rejected' WHERE [Order ID]='{ID}'";
            _db.AddUpdateDeleteRecord();
            dgvRightTable.DataSource = null;

            MessageBox.Show("Rejected Successfully.");
            UpdateLeftTable();
            ClearInput();
            btnRejectOrder.Enabled = false;
        }
    }

    private void dgvRightTable_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        if (!_mode.Equals("TransactionHistory"))
            btnDeleteItem.Enabled = true;
        else
            btnDeleteItem.Enabled = false;
    }

    private void btnDeleteItem_Click(object sender, EventArgs e)
    {
        var pwdPrompt = new frmPasswordPrompt();
        pwdPrompt.ShowDialog();
        var canEdit = pwdPrompt.canEdit;
        if (canEdit)
        {
            if (dgvRightTable.SelectedRows.Count == dgvRightTable.Rows.Count)
            {
                _db.Query = $@"DELETE FROM CustomerOrder WHERE [Order ID]='{ID}'";
                _db.AddUpdateDeleteRecord();
                UpdateLeftTable();
                ShowRightTableItem();
                return;
            }

            if (dgvRightTable.SelectedRows.Count != 0)
            {
                foreach (DataGridViewRow row in dgvRightTable.SelectedRows)
                {
                    var id = Convert.ToInt32(row.Cells[0].Value);
                    _db.Query = $@"DELETE FROM OrderDetails WHERE ProductID='{id}' AND OrderID='{ID}'";
                    _db.AddUpdateDeleteRecord();
                }


                UpdateLeftTable();
                ShowRightTableItem();
            }
            else
            {
                MessageBox.Show("Please Select the item");
            }
        }
    }

    private void btnLogout_Click(object sender, EventArgs e)
    {
        CloseWindow();
    }

    private void txtSearchOrder_KeyUp(object sender, KeyEventArgs e)
    {
        if (_mode.Equals("Transaction"))
            _db.Query = $@"SELECT [Order ID], Status, [Transaction Date] FROM CustomerOrder 
                        WHERE ([Order ID] LIKE '%{txtSearchOrder.Text.Trim()}%' 
                        OR Status LIKE '%{txtSearchOrder.Text.Trim()}%'
                        OR [Transaction Date] LIKE '%{txtSearchOrder.Text.Trim()}%')
                        AND Status='Pending' AND NOT Status='Accepted'
                        AND NOT Status='Rejected'                        
                        AND EmployeeID='{_employee.Id}'";
        else
            _db.Query = $@"SELECT [Order ID], Status, [Transaction Date] FROM CustomerOrder 
                        WHERE ([Order ID] LIKE '%{txtSearchOrder.Text.Trim()}%' 
                        OR Status LIKE '%{txtSearchOrder.Text.Trim()}%'
                        OR [Transaction Date] LIKE '%{txtSearchOrder.Text.Trim()}%')                      
                        AND EmployeeID='{_employee.Id}'";

        dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];
    }

    private void ClearInput()
    {
        txtCustomerMoney.Text = "";
        _totalPrice = 0;
        _customerExchange = 0;
        btnProcessOrder.Enabled = false;
        lblExchange.Text = "0 PHP";
        lblTotalPrice.Text = "0 PHP";
    }

    private void txtCustomerMoney_KeyUp(object sender, KeyEventArgs e)
    {
        try
        {
            _customerMoney = Convert.ToDouble(txtCustomerMoney.Text.Trim());

            _customerExchange = _customerMoney - _totalPrice;
            lblExchange.Text = $"{_customerExchange} PHP";
            if (_customerMoney >= _totalPrice)
                btnProcessOrder.Enabled = true;
            else
                btnProcessOrder.Enabled = false;
        }
        catch (Exception)
        {
            btnProcessOrder.Enabled = false;
        }
    }

    private void tmrRefreshData_Tick(object sender, EventArgs e)
    {
        if (dgvRightTable.DataSource == null)
        {
            if (!pctrBoxLeftTable.Visible) pctrBoxLeftTable.Visible = true;
        }
        else
        {
            pctrBoxLeftTable.Visible = false;
        }

        if (dgvLeftTable.Rows.Count == 0 || dgvLeftTable.DataSource == null)
        {
            if (!pnlLeftTableMessage.Visible) pnlLeftTableMessage.Visible = true;
        }
        else
        {
            pnlLeftTableMessage.Visible = false;
        }

        switch (_mode)
        {
            case "Transaction":
                _db.Query = $@"SELECT [Order ID], Status,  [Transaction Date] FROM CustomerOrder 
                    WHERE EmployeeID='{_employee.Id}' AND Status='Pending'";
                dgvUpdatedCopy.DataSource = _db.getTableData("CustomerOrder").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount) UpdateLeftTable();
                break;
            case "TransactionHistory":
                _db.Query = $@"SELECT [Order ID], Status, [Transaction Date] FROM CustomerOrder 
                                WHERE EmployeeID={_employee.Id} AND NOT Status='Pending'";
                dgvUpdatedCopy.DataSource = _db.getTableData("CustomerOrder").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount) UpdateLeftTable();
                break;
            case "ViewInventory":

                _db.Query = @"SELECT ProductID, [Product Name], Price, Stocks, Description, 
                                [Expiration Date], Category.[Category Name]
                    FROM Product INNER JOIN Category ON Category.CategoryID = Product.CategoryID";
                dgvUpdatedCopy.DataSource = _db.getTableData("Product").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateLeftTable();
                    return;
                }

                for (var i = 0; i < dgvLocalCopy.RowCount; i++)
                for (var j = 0; j < dgvLocalCopy.ColumnCount; j++)
                    if (dgvLocalCopy.Rows[i].Cells[j].Value + "" != dgvUpdatedCopy.Rows[i].Cells[j].Value + "")
                    {
                        UpdateLeftTable();
                        return;
                    }

                break;
        }
    }
}
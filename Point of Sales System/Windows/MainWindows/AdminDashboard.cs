﻿using System.Data;
using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.Pop_ups;

namespace Point_of_Sales_System.Windows.MainWindows;

public partial class AdminDashboard : Form
{
    private readonly string[] invalidWords =
    {
        "fuck", "idiot", "stupid", "dick",
        "pota", "gago", "shit", "gago",
        "tangina", "pakshit", "ulol"
    };

    private bool _addMode;
    private readonly int _currentYear = DateTime.Now.Year;
    private readonly Database? _db;
    private readonly Employee _employee;
    private readonly int _minimumBirthYear = 1934;
    private string _mode = "";
    private Employee _newUpdateEmployee;
    private int _rightID;
    private int ID;
    private Product info;
    private string productName;

    public AdminDashboard()
    {
        InitializeComponent();
    }

    public AdminDashboard(Employee employee, Database? db)
    {
        _employee = employee;
        _db = db;
        InitializeComponent();
    }

    private void AdminDashboard_Load(object sender, EventArgs e)
    {
        chcbxShowPassword.Checked = false;
        lblAdminName.Text = $"{_employee.FirstName} {_employee.LastName}";
        _mode = "Transaction";
        UpdateValues(_mode);
        tmrRefreshData.Start();
        UpdateTotalMonthSale();
        _db.Query = @"SELECT DISTINCT EmployeeType FROM EmployeeType";
        cmbAccountType.DisplayMember = "EmployeeType";
        cmbAccountType.ValueMember = "EmployeeType";
        cmbAccountType.DataSource = _db.getTableData("EmployeeType").Tables[0];
    }

    private void UpdateValues(string mode)
    {
        switch (mode)
        {
            case "Transaction":
                _db.Query = @"SELECT [Order ID], CONCAT(Employee.[First Name],' ',Employee.[Last Name]) AS 'Full Name',
                                  [Transaction Date], Status, Exchange FROM CustomerOrder INNER JOIN Employee ON
                                  CustomerOrder.EmployeeID = Employee.EmployeeID
                                  WHERE NOT Status='Pending'";
                dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];
                _db.Query = @"SELECT DISTINCT Status FROM CustomerOrder WHERE Status='Accepted' OR Status='Rejected'";

                var row = new DataTable();
                row = _db.getTableData("CustomerOrder").Tables[0];
                var defaultRow = row.NewRow();
                defaultRow["Status"] = "All";
                row.Rows.InsertAt(defaultRow, 0);
                cmbShowOption.DisplayMember = "Status";
                cmbShowOption.ValueMember = "Status";

                cmbShowOption.DataSource = row;

                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                cmbShowOption.SelectedIndex = 0;
                break;
            case "EmployeeLogin":
                _db.Query = @"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS FullName,
                                EmployeeType FROM Employee";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];
                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                break;
            case "AddUpdDelAccount":
                _db.Query = @"SELECT EmployeeID, CONCAT([First Name],' ',[Last Name]) AS 'FullName',
                                EmployeeType FROM Employee;";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];
                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                break;
            case "AddUpdDelProduct":

                _db.Query = @"SELECT ProductID, [Product Name], Price, Stocks, Description, 
                                [Expiration Date], Category.[Category Name]
                    FROM Product INNER JOIN Category ON Category.CategoryID = Product.CategoryID";
                dgvLeftTable.DataSource = _db.getTableData("Product").Tables[0];
                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                break;
            case "ViewAccountUpdate":

                _db.Query =
                    @"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS 'Admin Names' FROM Employee WHERE EmployeeType='Admin'";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];
                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                break;
            case "ViewProductChanges":
                _db.Query =
                    @"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS 'Admin Names' FROM Employee WHERE EmployeeType='Admin'";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];
                dgvLocalCopy.DataSource = dgvLeftTable.DataSource;
                break;
        }

        dgvLeftTable.ClearSelection();
    }

    private void tmrRefreshData_Tick(object sender, EventArgs e)
    {
        dgvUpdatedCopy.DataSource = null;
        switch (_mode)
        {
            case "Transaction":

                _db.Query = @"SELECT [Order ID], CONCAT(Employee.[First Name],' ',Employee.[Last Name]) AS 'Full Name',
                                  [Transaction Date], Status, Exchange FROM CustomerOrder INNER JOIN Employee ON
                                  CustomerOrder.EmployeeID = Employee.EmployeeID
                                  WHERE NOT Status='Pending'";
                dgvUpdatedCopy.DataSource = _db.getTableData("CustomerOrder").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateValues(_mode);
                    UpdateTotalMonthSale();
                }

                break;
            case "AddUpdDelAccount":

                _db.Query = @"SELECT EmployeeID, CONCAT([First Name],' ',[Last Name]) AS 'FullName',
                                EmployeeType FROM Employee;";
                dgvUpdatedCopy.DataSource = _db.getTableData("Employee").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateValues(_mode);
                    return;
                }

                for (var i = 0; i < dgvLocalCopy.RowCount; i++)
                for (var j = 0; j < dgvLocalCopy.ColumnCount; j++)
                    if (dgvLocalCopy.Rows[i].Cells[j].Value + "" != dgvUpdatedCopy.Rows[i].Cells[j].Value + "")
                    {
                        UpdateValues(_mode);
                        return;
                    }

                break;
            case "AddUpdDelProduct":
                _db.Query = @"SELECT ProductID, [Product Name], Price, Stocks, Description, 
                                [Expiration Date], Category.[Category Name]
                                FROM Product INNER JOIN Category ON Category.CategoryID = Product.CategoryID;";
                dgvUpdatedCopy.DataSource = _db.getTableData("Product").Tables[0];
                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateValues(_mode);

                    return;
                }

                for (var i = 0; i < dgvLocalCopy.RowCount; i++)
                for (var j = 0; j < dgvLocalCopy.ColumnCount; j++)
                    if (dgvLocalCopy.Rows[i].Cells[j].Value + "" != dgvUpdatedCopy.Rows[i].Cells[j].Value + "")
                    {
                        UpdateValues(_mode);
                        return;
                    }

                break;
            case "ViewAccountUpdate":
                _db.Query =
                    @"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS 'Admin Names' FROM Employee WHERE EmployeeType='Admin'";
                dgvUpdatedCopy.DataSource = _db.getTableData("Employee").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateValues(_mode);

                    return;
                }

                for (var i = 0; i < dgvLocalCopy.RowCount; i++)
                for (var j = 0; j < dgvLocalCopy.ColumnCount; j++)
                    if (dgvLocalCopy.Rows[i].Cells[j].Value + "" != dgvUpdatedCopy.Rows[i].Cells[j].Value + "")
                    {
                        UpdateValues(_mode);
                        return;
                    }

                break;
            case "ViewProductChanges":
                _db.Query =
                    @"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS 'Admin Names' FROM Employee WHERE EmployeeType='Admin'";
                dgvUpdatedCopy.DataSource = _db.getTableData("Employee").Tables[0];

                if (dgvLocalCopy.RowCount != dgvUpdatedCopy.RowCount)
                {
                    UpdateValues(_mode);

                    return;
                }

                for (var i = 0; i < dgvLocalCopy.RowCount; i++)
                for (var j = 0; j < dgvLocalCopy.ColumnCount; j++)
                    if (dgvLocalCopy.Rows[i].Cells[j].Value + "" != dgvUpdatedCopy.Rows[i].Cells[j].Value + "")
                    {
                        UpdateValues(_mode);
                        return;
                    }

                break;
        }
    }

    private void AdminDashboard_FormClosing(object sender, FormClosingEventArgs e)
    {
        e.Cancel = CloseWindow();
    }

    private bool CloseWindow()
    {
        var res = MessageBox.Show("Are you sure you want to logout?",
            "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            _employee.IsLoggedIn = false;
            _db.Query = $@"UPDATE EmployeeCredential SET LoggedIn={Convert.ToInt32(_employee.IsLoggedIn)} 
                            WHERE [User Name]='{_employee.UserName}'";
            _db.AddUpdateDeleteRecord();
            var login = new EmployeeLogin(_db);
            login.Show();
            Dispose();
        }

        return true;
    }

    private void UpdateTotalMonthSale()
    {
        _db.Query = $@"SELECT SUM(OrderDetails.Total) AS 'TotalSales' FROM CustomerOrder 
                        INNER JOIN OrderDetails ON CustomerOrder.[Order ID] = OrderDetails.OrderID 
                        WHERE MONTH([Transaction Date])=MONTH('{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}') 
                        AND Status='Accepted'";

        var data = $"P{_db.returnTotalSale().ToString()}";

        lblTotalSales.Text = data;
    }

    private void dgvLeftTable_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        pnlAddUpdMessage.Visible = false;
        pnlAddUpdDelProductMessage.Visible = false;
        btnDeleteAccount.Enabled = true;
        try
        {
            switch (_mode)
            {
                case "Transaction":
                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    break;

                case "EmployeeLogin":
                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    btnViewEmployeeDetails.Enabled = true;
                    break;
                case "AddUpdDelAccount":
                    _addMode = false;
                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    break;
                case "AddUpdDelProduct":
                    btnCreateProduct.Enabled = false;
                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    productName = dgvLeftTable.Rows[e.RowIndex].Cells[6].Value + "";
                    _addMode = false;
                    break;
                case "ViewAccountUpdate":

                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    _db.Query = $@"SELECT * FROM AccountUpdateHistory WHERE [User Name]=(
                                    SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";
                    break;
                case "ViewProductChanges":
                    ID = Convert.ToInt32(dgvLeftTable.Rows[e.RowIndex].Cells[0].Value + "");
                    _db.Query = $@"SELECT * FROM InventoryHistory WHERE EmployeeID='{ID}'";
                    break;
            }

            ShowRightTableInfo(_mode, ID);
        }
        catch (Exception)
        {
        }
    }

    private bool IsAccountValid()
    {
        string year = "", month = "", date = "";
        switch (_mode)
        {
            case "AddUpdDelAccount":

                year = cmbBirthYear.SelectedItem.ToString();
                month = cmbMonth.SelectedItem.ToString();
                date = cmbDate.SelectedItem.ToString();
                if (string.IsNullOrEmpty(txtFirstName.Text.Trim()) ||
                    string.IsNullOrEmpty(txtFirstName.Text.Trim()) ||
                    string.IsNullOrEmpty(txtLastName.Text.Trim()) ||
                    string.IsNullOrEmpty(txtAddress.Text.Trim()) ||
                    string.IsNullOrEmpty(txtPassword.Text.Trim()) ||
                    string.IsNullOrEmpty(year.Trim()) ||
                    string.IsNullOrEmpty(month.Trim()) ||
                    string.IsNullOrEmpty(date.Trim())
                   )
                {
                    MessageBox.Show("Please fill out all the fields.");
                    return false;
                }

                var firstName = txtFirstName.Text.Trim();
                var middleName = txtMiddleName.Text.Trim();
                var lastName = txtLastName.Text.Trim();
                var address = txtAddress.Text.Trim();
                var password = txtPassword.Text.Trim();
                var accountType = cmbAccountType.Text;
                var userName = txtUserName.Text.Trim();

                if (userName.Contains(" "))
                {
                    MessageBox.Show("Username cannot contains whitespaces.");
                    return false;
                }

                foreach (var badWords in invalidWords)
                    if (string.Equals(badWords, firstName, StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(badWords, middleName, StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(badWords, lastName, StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(badWords, address, StringComparison.OrdinalIgnoreCase)
                       )
                    {
                        MessageBox.Show("Input should not contain bad words");
                        return false;
                    }

                string gender;
                string status;
                if (rdbMale.Checked | rdbFemale.Checked && rdbAccountEnable.Checked | rdbAccountDisable.Checked)
                {
                    if (rdbMale.Checked)
                        gender = rdbMale.Text;
                    else
                        gender = rdbFemale.Text;

                    if (rdbAccountEnable.Checked)
                        status = rdbAccountEnable.Text;
                    else
                        status = rdbAccountDisable.Text;
                }
                else
                {
                    MessageBox.Show("Some radio button is not checked.");
                    return false;
                }

                _newUpdateEmployee = new Employee();
                _newUpdateEmployee.ProfilePicture = pctrEmployee.ImageLocation;
                _newUpdateEmployee.FirstName = firstName;
                _newUpdateEmployee.MiddleName = middleName;
                _newUpdateEmployee.LastName = lastName;
                _newUpdateEmployee.Gender = gender;
                _newUpdateEmployee.Address = address;
                _newUpdateEmployee.BirthDate = string.Join("/", year, month, date);
                _newUpdateEmployee.EmployeeType = accountType;
                _newUpdateEmployee.UserName = userName;
                _newUpdateEmployee.Password = password;
                _newUpdateEmployee.AccountStatus = status;


                break;
            case "AddUpdDelProduct":

                info = new Product();
                info.ProductName = txtProductName.Text.Trim();
                info.CategoryName = cmbProductType.SelectedValue + "";
                year = cmbProductYear.SelectedItem.ToString();
                month = cmbProductMonth.SelectedItem.ToString();
                date = cmbProductDate.SelectedItem.ToString();
                info.Expiration_Date = string.Join("/", month, date, year);
                info.Description = txtProductDescription.Text.Trim();
                try
                {
                    info.Price = Convert.ToDouble(txtPrice.Text.Trim());
                    info.Stocks = Convert.ToInt32(txtStocks.Text.Trim());
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a correct number.");
                    return false;
                }

                if (string.IsNullOrEmpty(info.ProductName)
                    || string.IsNullOrEmpty(info.CategoryName)
                    || string.IsNullOrEmpty(info.Expiration_Date))
                {
                    MessageBox.Show("Please fill out all the fields", "Attention",
                        MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return false;
                }


                break;
        }

        return true;
    }

    private void ShowRightTableInfo(string mode, int id)
    {
        var values = "";
        switch (mode)
        {
            case "Transaction":
                _db.Query = $@"SELECT Product.ProductID, Product.[Product Name], OrderDetails.Quantity, 
                        OrderDetails.Total FROM OrderDetails INNER JOIN 
                        Product ON OrderDetails.ProductID = Product.ProductID WHERE OrderID={id};";
                dgvRightTable.DataSource = _db.getTableData("OrderDetails").Tables[0];

                double totalPrice = 0;
                foreach (DataGridViewRow row in dgvRightTable.Rows) totalPrice += Convert.ToDouble(row.Cells[3].Value);

                lblTotalPrice.Text = "₱" + totalPrice.ToString("#.##");
                break;
            case "EmployeeLogin":
                _db.Query = $@"SELECT Employee.EmployeeID, 	
	                            CONCAT(Employee.[First Name], ' ', Employee.[Last Name]) 
                                AS FullName, CONCAT('Logged in at ', DateTime) AS 'LoginInfo' FROM LoginHistory
                                INNER JOIN Employee ON LoginHistory.[User Name] = Employee.[User Name]
	                            WHERE LoginHistory.[User Name]= (SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";


                dgvRightTable.DataSource = _db.getTableData("LoginHistory").Tables[0];
                lblRightTable.Text = "Login Count";
                var totalCount = dgvRightTable.RowCount;
                lblLoginCount.Text = $@"{totalCount}";
                break;
            case "AddUpdDelAccount":
                ClearAllInput();
                if (!_addMode)
                {
                    btnCreateAccount.Enabled = false;

                    _db.Query =
                        $@"SELECT EmployeeID, [Profile Picture], [First Name], [Middle Name], [Last Name], Address, Gender,
                                    CONCAT(YEAR([Birth Date]),'/',MONTH([Birth Date]),'/',DAY([Birth Date])) AS 'BirthDate', 
                                    Employee.[User Name], EmployeeType, AccountStatus, EmployeeCredential.Password
                                    FROM Employee INNER JOIN EmployeeCredential ON EmployeeCredential.[User Name] = Employee.[User Name] 
                                    WHERE Employee.[User Name]= (SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";
                    var info = _db.getEmployeeRecord()[0];
                    if (!string.IsNullOrEmpty(info.ProfilePicture))
                        pctrEmployee.Image = Image.FromFile(info.ProfilePicture);
                    txtFirstName.Text = info.FirstName;
                    txtMiddleName.Text = info.MiddleName;
                    txtLastName.Text = info.LastName;
                    if (info.Gender.Trim().Equals("Male"))
                    {
                        rdbMale.Checked = true;
                    }
                    else
                    {
                        rdbMale.Checked = false;
                        rdbFemale.Checked = true;
                    }

                    txtAddress.Text = info.Address;
                    var birthDate = info.BirthDate.Split('/');
                    //this loops is not efficient because it causes lag 
                    for (var i = 0; i < cmbBirthYear.Items.Count; i++)
                    {
                        values = cmbBirthYear.Items[i].ToString();
                        if (values.Equals(birthDate[0]))
                        {
                            cmbBirthYear.SelectedIndex = i;
                            break;
                        }
                    }

                    values = "";
                    for (var i = 0; i < cmbMonth.Items.Count; i++)
                    {
                        values = cmbMonth.Items[i].ToString();
                        if (values.Equals(birthDate[1]))
                        {
                            cmbMonth.SelectedIndex = i;
                            break;
                        }
                    }

                    for (var i = 0; i < cmbDate.Items.Count; i++)
                    {
                        values = cmbDate.Items[i].ToString();
                        if (values.Equals(birthDate[2]))
                        {
                            cmbDate.SelectedIndex = i;
                            break;
                        }
                    }

                    _db.Query =
                        $@"SELECT [User Name], Password, LoggedIn FROM EmployeeCredential WHERE [User Name]='{info.UserName}';";
                    txtUserName.Text = info.UserName;

                    info.Password = _db.GetEmployee("EmployeeCredential").Password;
                    info.IsLoggedIn = _db.GetEmployee("EmployeeCredential").IsLoggedIn;
                    txtPassword.Text = info.Password;
                    cmbAccountType.SelectedValue = info.EmployeeType;
                    if (info.AccountStatus.Equals("Enable"))
                    {
                        rdbAccountEnable.Checked = true;
                    }
                    else
                    {
                        rdbAccountEnable.Checked = false;
                        rdbAccountDisable.Checked = true;
                    }

                    txtUserName.Enabled = false;
                }
                else
                {
                    btnDeleteAccount.Enabled = false;
                    btnUpdateAccount.Enabled = false;
                    btnCreateAccount.Enabled = true;
                    btnAddNewAccount.Enabled = false;
                    txtUserName.Enabled = true;
                }

                break;
            case "AddUpdDelProduct":
                if (!_addMode)
                {
                    btnUpdateProduct.Enabled = true;
                    btnDeleteProduct.Enabled = true;
                    _db.Query = $@"SELECT * FROM Product WHERE ProductID='{ID}'";
                    var info = _db.getProductRecord();
                    txtProductName.Text = info.ProductName;
                    txtPrice.Text = info.Price + "";
                    txtStocks.Text = info.Stocks + "";
                    var parsedProductDate = info.Expiration_Date.Split('/');
                    values = "";
                    parsedProductDate[2] = parsedProductDate[2].Substring(0, parsedProductDate[2].IndexOf(" ")).Trim();
                    for (var i = 0; i < cmbProductYear.Items.Count; i++)
                    {
                        values = cmbProductYear.Items[i].ToString();
                        if (values.Equals(parsedProductDate[2]))
                        {
                            cmbProductYear.SelectedIndex = i;
                            break;
                        }
                    }

                    values = "";
                    for (var i = 0; i < cmbProductMonth.Items.Count; i++)
                    {
                        values = cmbProductMonth.Items[i].ToString();
                        if (values.Equals(parsedProductDate[0]))
                        {
                            cmbProductMonth.SelectedIndex = i;
                            break;
                        }
                    }

                    values = "";
                    for (var i = 0; i < cmbProductDate.Items.Count; i++)
                    {
                        values = cmbProductDate.Items[i].ToString();
                        if (values.Equals(parsedProductDate[1]))
                        {
                            cmbProductDate.SelectedIndex = i;
                            break;
                        }
                    }

                    txtProductDescription.Text = info.Description;

                    cmbProductType.SelectedValue = productName.Trim();
                }
                else
                {
                    btnCreateProduct.Enabled = true;
                    btnUpdateProduct.Enabled = false;
                    btnDeleteProduct.Enabled = false;
                }

                break;
            case "ViewAccountUpdate":

                dgvRightTable.DataSource = _db.getTableData("AccountUpdateHistory").Tables[0];
                if (dgvRightTable.Rows.Count == 0)
                    btnViewEmployeeDetails.Enabled = false;
                else
                    btnViewEmployeeDetails.Enabled = true;
                break;
            case "ViewProductChanges":
                dgvRightTable.DataSource = _db.getTableData("InventoryHistory").Tables[0];

                break;
        }

        dgvRightTable.ClearSelection();
    }

    private void ClearAllInput()
    {
        txtUserName.Text = "";
        txtPassword.Text = "";
        txtFirstName.Text = "";
        txtMiddleName.Text = "";
        txtLastName.Text = "";
        txtAddress.Text = "";
        rdbAccountDisable.Checked = false;
        rdbAccountEnable.Checked = false;
        rdbFemale.Checked = false;
        rdbMale.Checked = false;
        cmbAccountType.SelectedIndex = 0;
        txtAccountReason.Text = "";
        if (dgvLeftTable.SelectedRows.Count == 0)
            btnDeleteAccount.Enabled = false;
        btnUpdateAccount.Enabled = true;
        btnCreateAccount.Enabled = false;
        btnAddNewAccount.Enabled = true;
        cmbBirthYear.SelectedIndex = 0;
        cmbMonth.SelectedIndex = 0;
        cmbDate.SelectedIndex = 0;
    }

    private void txtCashierSearch_KeyUp(object sender, KeyEventArgs e)
    {
        var input = txtCashierSearch.Text.Trim();

        switch (_mode)
        {
            case "Transaction":
                cmbShowOption.SelectedIndex = 0;
                _db.Query = $@"SELECT [Order ID], CONCAT(Employee.[First Name],' ',Employee.[Last Name]) AS 'Full Name',
                                [Transaction Date], Status, Exchange FROM CustomerOrder INNER JOIN Employee ON
                                CustomerOrder.EmployeeID = Employee.EmployeeID WHERE Employee.[First Name] 
                                LIKE '%{input}%' OR Employee.[Last Name] LIKE '%{input}%'";
                dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];
                break;
            case "EmployeeLogin":
                _db.Query = $@"SELECT EmployeeID, CONCAT([First Name], ' ', [Last Name]) AS FullName,
                                EmployeeType FROM Employee 
                                WHERE [First Name] LIKE '%{input}%' or [Last Name] LIKE '%{input}%' OR
                                EmployeeID LIKE '%{input}%' OR EmployeeType LIKE '%{input}%'";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];
                break;
            case "AddUpdDelAccount":
                _db.Query = $@"SELECT EmployeeID, CONCAT([First Name],' ',[Last Name]) AS 'FullName',
                                EmployeeType FROM Employee
                                WHERE [First Name] LIKE '%{input}%' or [Last Name] LIKE '%{input}%' OR
                                EmployeeID LIKE '%{input}%' OR EmployeeType LIKE '%{input}%'";
                dgvLeftTable.DataSource = _db.getTableData("Employee").Tables[0];

                break;
        }
    }


    private void cmbShowOption_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbShowOption.SelectedIndex != 0)
        {
            var option = cmbShowOption.Text;

            _db.Query = $@"SELECT [Order ID], CONCAT(Employee.[First Name],' ',Employee.[Last Name]) AS 'Full Name',
                [Transaction Date], Status, Exchange
                FROM CustomerOrder INNER JOIN Employee ON
                CustomerOrder.EmployeeID = Employee.EmployeeID WHERE CustomerOrder.Status='{option}'";
        }
        else
        {
            _db.Query = @"SELECT [Order ID], CONCAT(Employee.[First Name],' ',Employee.[Last Name]) AS 'Full Name',
                [Transaction Date], Status, Exchange
                FROM CustomerOrder INNER JOIN Employee ON
                CustomerOrder.EmployeeID = Employee.EmployeeID WHERE NOT Status='Pending'";
        }

        dgvLeftTable.DataSource = _db.getTableData("CustomerOrder").Tables[0];
    }

    private void button5_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Are you sure you want to delete this record? "
            , "Confirmation",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        if (res == DialogResult.Yes)
        {
            switch (_mode)
            {
                case "Transaction":
                    /*_db.Query = $@"DELETE FROM TransactionHistory WHERE OrderID='{ID}'";
                    _db.AddUpdateDeleteRecord();*/
                    _db.Query = $@"DELETE FROM OrderDetails WHERE OrderID='{ID}'";
                    _db.AddUpdateDeleteRecord();
                    _db.Query = $@"DELETE FROM CustomerOrder WHERE [Order ID]='{ID}'";
                    _db.AddUpdateDeleteRecord();
                    break;
                case "EmployeeLogin":

                    _db.Query = $@"DELETE FROM LoginHistory WHERE [User Name]=
                                    (SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";
                    _db.AddUpdateDeleteRecord();
                    lblLoginCount.Text = "";
                    break;
                case "ViewAccountUpdate":
                    _db.Query = $@"DELETE FROM AccountUpdateHistory WHERE [User Name]=
                                    (SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";
                    _db.AddUpdateDeleteRecord();

                    break;
                case "ViewProductChanges":
                    _db.Query = $@"DELETE FROM InventoryHistory WHERE EmployeeID={ID}";
                    _db.AddUpdateDeleteRecord();
                    break;
            }

            dgvRightTable.DataSource = null;
            _rightID = 0;
            MessageBox.Show("Deleted Successfully.");
        }
    }

    private void button1_Click(object sender, EventArgs e)
    {
        pnlTransactionOption.Visible = false;
        dgvRightTable.DataSource = null;
        pnlAddUpdDelProductOption.Visible = false;
        lblLoginCount.Visible = true;
        lblLoginCount.Text = "";
        btnViewEmployeeDetails.Visible = true;
        btnViewEmployeeDetails.Enabled = false;
        lblLeftTable.Text = "Login History";
        lblRightTotalPrice.Visible = false;
        lblTotalPrice.Visible = false;
        _mode = "EmployeeLogin";
        pnlTransactionOption.Visible = false;
        ResetAddUpdValues("ViewEmployeeMode");
        lblRightTable.Text = "No of login";
        UpdateValues(_mode);
    }

    private void btnViewTransaction_Click(object sender, EventArgs e)
    {
        dgvRightTable.DataSource = null;
        txtCashierSearch.Visible = true;
        lblSearchCashier.Visible = true;
        _mode = "Transaction";
        UpdateTotalMonthSale();
        lblLoginCount.Visible = false;
        lblTotalPrice.Visible = true;
        lblRightTotalPrice.Visible = true;
        lblLeftTable.Text = "Transaction History";

        pnlReport.Visible = true;
        lblRightTable.Text = "Transaction Details";

        ResetAddUpdValues("Transaction");
        pnlAddSomething.Visible = false;
        UpdateValues(_mode);
    }

    private void label10_Click(object sender, EventArgs e)
    {
    }

    private void btnViewReports_Click(object sender, EventArgs e)
    {
    }


    private void button2_Click(object sender, EventArgs e)
    {
        pnlTransactionOption.Visible = false;
        btnViewEmployeeDetails.Visible = false;
        btnViewEmployeeDetails.Enabled = false;
        txtCashierSearch.Visible = true;
        lblSearchCashier.Visible = true;
        pnlAddUpdDelProductOption.Visible = false;
        lblLeftTable.Text = "Account List";
        pnlAddSomething.Visible = true;
        pnlAddUpdOption.Visible = true;
        pnlRightTableOptions.Visible = false;
        pnlReport.Visible = false;

        _mode = "AddUpdDelAccount";
        cmbBirthYear.Items.Clear();
        for (var i = _minimumBirthYear; i <= _currentYear; i++) cmbBirthYear.Items.Add(i);
        cmbMonth.Items.Clear();
        for (var i = 1; i <= 12; i++) cmbMonth.Items.Add(i);
        cmbBirthYear.SelectedIndex = 0;
        cmbMonth.SelectedIndex = 0;

        ResetAddUpdValues("AddUpdDelEmployeeMode");
        ClearAllInput();
        UpdateValues(_mode);
    }

    private void ResetAddUpdValues(string panelName)
    {
        if (panelName.Equals("AddUpdDelProductMode"))
        {
            pnlAddUpdDelProductOption.Visible = true;
            pnlReport.Visible = false;
            pnlAddUpdOption.Visible = false;
            pnlTransactionOption.Visible = false;
            pnlAddUpdDeleteProduct.Visible = true;
            pnlAddSomething.Visible = false;
            _db.Query = @"SELECT CategoryID AS 'CategoryID', [Category Name] AS 'CategoryName' 
                                FROM Category";

            cmbProductType.ValueMember = "CategoryName";
            cmbProductType.DisplayMember = "CategoryName";
            cmbProductType.DataSource = _db.getTableData("Category").Tables[0];
        }
        else if (panelName.Equals("AddUpdDelEmployeeMode"))
        {
            lblLoginCount.Visible = false;
            pnlAddUpdDelProductOption.Visible = false;
            pnlReport.Visible = false;
            pnlAddUpdOption.Visible = true;
            pnlTransactionOption.Visible = false;
            pnlAddUpdDeleteProduct.Visible = false;
            pnlAddSomething.Visible = true;
        }
        else if (panelName.Equals("Transaction"))
        {
            btnViewEmployeeDetails.Visible = false;
            pnlAddUpdDelProductOption.Visible = false;
            pnlReport.Visible = true;
            pnlAddUpdOption.Visible = false;
            pnlTransactionOption.Visible = false;
            pnlRightTableOptions.Visible = true;
            pnlAddSomething.Visible = false;
            pnlAddUpdDeleteProduct.Visible = false;
            pnlTransactionOption.Visible = true;
        }
        else if (panelName.Equals("ViewEmployeeMode"))
        {
            pnlAddUpdDelProductOption.Visible = false;
            pnlReport.Visible = false;
            pnlAddUpdOption.Visible = false;
            pnlTransactionOption.Visible = false;
            pnlRightTableOptions.Visible = true;
            pnlAddSomething.Visible = false;
            pnlAddUpdDeleteProduct.Visible = false;
        }
    }

    private void btnAddNewAccount_Click(object sender, EventArgs e)
    {
        dgvLeftTable.ClearSelection();
        _addMode = true;
        ShowRightTableInfo(_mode, ID);
    }

    private void label11_Click(object sender, EventArgs e)
    {
    }

    private void button7_Click(object sender, EventArgs e)
    {
        AddUpdateDelete("Accounts", "Update", ID);
    }

    private void AddUpdateDelete(string entityName, string type, int ID)
    {
        switch (entityName)
        {
            case "Product":
                break;
            case "Accounts":
                switch (type)
                {
                    case "Update":
                        if (IsAccountValid())
                        {
                            var res = MessageBox.Show("Are you sure you want to update this account?",
                                "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (res == DialogResult.Yes)
                            {
                                _db.Query = $@"UPDATE EmployeeCredential SET Password='{_newUpdateEmployee.Password}'
                                WHERE [User Name]='{_newUpdateEmployee.UserName}';
                                ";
                                _db.AddUpdateDeleteRecord();


                                _db.Query = $@"UPDATE Employee SET [Profile Picture]='{_employee.ProfilePicture}',
                                [First Name]='{_newUpdateEmployee.FirstName}', [Middle Name]='{_newUpdateEmployee.MiddleName}', 
					            [Last Name]='{_newUpdateEmployee.LastName}', Address='{_newUpdateEmployee.Address}', 
                                Gender='{_newUpdateEmployee.Gender}', [Birth Date]='{_newUpdateEmployee.BirthDate}', 
                                EmployeeType='{_newUpdateEmployee.EmployeeType}', AccountStatus='{_newUpdateEmployee.AccountStatus}' 
					            WHERE EmployeeID='{ID}'";
                                _db.AddUpdateDeleteRecord();
                                _db.Query = $@"INSERT INTO AccountUpdateHistory VALUES('{ID}','{_employee.UserName}',
                                 '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}','{type}','{txtAccountReason.Text}')";
                                _db.AddUpdateDeleteRecord();

                                ClearAllInput();

                                lblAddUpdText.Text = "Account has been updated.";
                                pnlAddUpdMessage.Visible = true;
                                UpdateValues(_mode);
                            }
                        }

                        break;
                    case "Add":
                        if (IsAccountValid())
                        {
                            _db.Query =
                                $@"SELECT * FROM EmployeeCredential WHERE [User Name]='{_newUpdateEmployee.UserName}'";
                            var info = _db.GetEmployee("EmployeeCredential");

                            if (info == null)
                            {
                                _db.Query = $@"INSERT INTO EmployeeCredential VALUES 
                                                ('{_newUpdateEmployee.UserName}', '{_newUpdateEmployee.Password}',
                                                '0')";
                                _db.AddUpdateDeleteRecord();
                                _db.Query = $@"INSERT INTO Employee (
                                                [Profile Picture], 
					                            [First Name], 
					                            [Middle Name], 
					                            [Last Name],
					                            Address, 
					                            Gender, 
					                            [Birth Date], 
					                            [User Name], 
					                            EmployeeType,
					                            AccountStatus)
                                    VALUES ('{_newUpdateEmployee.ProfilePicture}','{_newUpdateEmployee.FirstName}',
                                            '{_newUpdateEmployee.MiddleName}', '{_newUpdateEmployee.LastName}',
                                            '{_newUpdateEmployee.Address}', '{_newUpdateEmployee.Gender}',
                                            '{_newUpdateEmployee.BirthDate}', '{_newUpdateEmployee.UserName}',
                                            '{_newUpdateEmployee.EmployeeType}', '{_newUpdateEmployee.AccountStatus}')";


                                _db.AddUpdateDeleteRecord();
                                _db.Query =
                                    $@"SELECT EmployeeID, [Profile Picture], [First Name], [Middle Name], [Last Name], Address, Gender,
                                    CONCAT(YEAR([Birth Date]),'/',MONTH([Birth Date]),'/',DAY([Birth Date])) AS 'BirthDate', 
                                    Employee.[User Name], EmployeeType, AccountStatus, EmployeeCredential.Password
                                    FROM Employee INNER JOIN EmployeeCredential ON EmployeeCredential.[User Name] = Employee.[User Name] 
                                    WHERE Employee.[User Name]= (SELECT [User Name] FROM Employee WHERE [User Name]='{_newUpdateEmployee.UserName}')";
                                var iD = _db.getEmployeeRecord()[0].Id;

                                _db.Query = $@"INSERT INTO AccountUpdateHistory VALUES('{iD}','{_employee.UserName}',
                                 '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}','{type}','{txtAccountReason.Text}')";
                                _db.AddUpdateDeleteRecord();
                                lblAddUpdText.Text = "Account Created!";

                                pnlAddUpdMessage.Visible = true;
                                ClearAllInput();
                            }
                            else
                            {
                                MessageBox.Show("Username already taken. Please choose Another.",
                                    "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }

                        break;
                }

                break;
        }
    }

    private void btnCreateAccount_Click(object sender, EventArgs e)
    {
        AddUpdateDelete("Accounts", "Add", ID);
    }

    private void btnDeleteAccount_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Do you want to delete this account?", "Warning",
            MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        if (res == DialogResult.Yes)
        {
            _db.Query =
                $@"SELECT EmployeeID, [Profile Picture], [First Name], [Middle Name], [Last Name], Address, Gender,
                                    CONCAT(YEAR([Birth Date]),'/',MONTH([Birth Date]),'/',DAY([Birth Date])) AS 'BirthDate', 
                                    Employee.[User Name], EmployeeType, AccountStatus, EmployeeCredential.Password
                                    FROM Employee INNER JOIN EmployeeCredential ON EmployeeCredential.[User Name] = Employee.[User Name] 
                                    WHERE Employee.[User Name]= (SELECT [User Name] FROM Employee WHERE EmployeeID='{ID}')";
            var info = _db.getEmployeeRecord()[0];
            _db.Query =
                $@"SELECT [User Name], Password, LoggedIn FROM EmployeeCredential WHERE [User Name]='{info.UserName}';";
            info.Password = _db.GetEmployee("EmployeeCredential").Password;
            info.IsLoggedIn = _db.GetEmployee("EmployeeCredential").IsLoggedIn;
            if (!info.IsLoggedIn)
            {
                _db.Query = $@"DELETE FROM Employee WHERE EmployeeID={info.Id}";
                _db.AddUpdateDeleteRecord();
                _db.Query = @"UPDATE ";
                _db.Query = $@"DELETE FROM EmployeeCredential WHERE [User Name]='{info.UserName}'";
                _db.AddUpdateDeleteRecord();
            }
            else
            {
                MessageBox.Show("Cannot delete an account while the user is logged in.");
            }
        }

        btnDeleteAccount.Enabled = false;
        ClearAllInput();
    }

    private void button3_Click(object sender, EventArgs e)
    {
        pnlTransactionOption.Visible = false;
        txtCashierSearch.Visible = false;
        lblSearchCashier.Visible = false;
        lblLeftTable.Text = "Product List";
        pnlRightTableOptions.Visible = false;
        ResetAddUpdValues("AddUpdDelProductMode");
        _mode = "AddUpdDelProduct";
        var maxYear = _currentYear + 10;
        cmbProductYear.Items.Clear();
        cmbProductMonth.Items.Clear();
        cmbProductDate.Items.Clear();


        for (var i = _currentYear; i <= maxYear; i++) cmbProductYear.Items.Add(i);
        cmbProductYear.SelectedIndex = 0;

        for (var i = 1; i <= 12; i++) cmbProductMonth.Items.Add(i);

        cmbProductMonth.SelectedIndex = 0;
        UpdateValues(_mode);
    }

    private void pnlAddUpdDeleteProduct_Paint(object sender, PaintEventArgs e)
    {
    }

    private void btnAddNewProduct_Click(object sender, EventArgs e)
    {
        dgvLeftTable.ClearSelection();
        _addMode = true;
        ClearProductInput();
        ShowRightTableInfo(_mode, ID);
    }

    private void btnUpdateProduct_Click(object sender, EventArgs e)
    {
        if (IsAccountValid())
        {
            var res = MessageBox.Show("Are you sure you want to update this product?", "Question",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                if (string.IsNullOrEmpty(info.Description))
                    info.Description = "No Description Given";
                _db.Query = $@"UPDATE Product SET [Product Picture]='', [Product Name]='{info.ProductName}', 
					Price='{info.Price}', Stocks='{info.Stocks}', Description='{info.Description}', 
					[Expiration Date]='{info.Expiration_Date}', CategoryID=(SELECT CategoryID FROM Category WHERE [Category Name]='{info.CategoryName}')
					WHERE ProductID='{ID}' ";
                _db.AddUpdateDeleteRecord();
                var reason = txtProductReason.Text.Trim();
                if (string.IsNullOrEmpty(reason))
                    reason = "No Reason Given";
                _db.Query = $@"INSERT INTO InventoryHistory VALUES
                    ('{_employee.Id}','UPDATE', '{ID}', '{reason}', '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}')";
                _db.AddUpdateDeleteRecord();
                lblProductStatus.Text = "Product Updated!";
                pnlAddUpdDelProductMessage.Visible = true;
            }
        }
    }

    private void ClearProductInput()
    {
        txtProductName.Text = "";
        txtStocks.Text = "";
        txtPrice.Text = "";
        txtProductDescription.Text = "";
        cmbProductDate.SelectedIndex = 0;
        cmbProductMonth.SelectedIndex = 0;
        cmbProductYear.SelectedIndex = 0;
        txtProductReason.Text = "";
    }

    private void btnCreateProduct_Click(object sender, EventArgs e)
    {
        if (IsAccountValid())
        {
            var res = MessageBox.Show("Are you sure to add this product?", "Question",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                _db.Query = $@"INSERT Product([Product Name], Price, Stocks, [Expiration Date], CategoryID) VALUES(
                                '{info.ProductName}',
                                '{info.Price}',
                                '{info.Stocks}',
                                '{info.Expiration_Date}',
                                (SELECT CategoryID FROM Category WHERE [Category Name]='{info.CategoryName}')
                                )";
                _db.AddUpdateDeleteRecord();

                _db.Query = @"SELECT * FROM Product";
                var id = _db.getProductRecord().Id;
                var reason = txtProductReason.Text.Trim();
                if (string.IsNullOrEmpty(reason))
                    reason = "No Reason Given";
                _db.Query = $@"INSERT INTO InventoryHistory VALUES
                    ('{_employee.Id}','UPDATE', '{id}', '{reason}', '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}')";
                _db.AddUpdateDeleteRecord();
                lblProductStatus.Text = "Product Added!";
                pnlAddUpdDelProductMessage.Visible = true;
                ClearProductInput();
            }
        }
    }

    private void btnDeleteProduct_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Are you sure to delete this product?", "Question",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (res == DialogResult.Yes)
        {
            _db.Query = $@"DELETE FROM Product WHERE ProductID='{ID}'";

            _db.AddUpdateDeleteRecord();
            lblProductStatus.Text = "Product Deleted!";
            pnlAddUpdDelProductMessage.Visible = true;
            ClearProductInput();
        }
    }

    private void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateComboBox();
    }

    private void UpdateComboBox()
    {
        switch (_mode)
        {
            case "AddUpdDelAccount":
                if (cmbMonth.SelectedIndex >= 0 && cmbBirthYear.SelectedIndex >= 0)
                {
                    cmbDate.Items.Clear();

                    var month = Convert.ToInt32(cmbMonth.SelectedItem);
                    var year = Convert.ToInt32(cmbBirthYear.SelectedItem);
                    var days = DateTime.DaysInMonth(year, month);
                    for (var i = 1; i <= days; i++) cmbDate.Items.Add(i);

                    cmbDate.SelectedIndex = 0;
                }

                break;

            case "AddUpdDelProduct":

                if (cmbProductYear.SelectedIndex >= 0 && cmbProductMonth.SelectedIndex >= 0)
                {
                    cmbProductDate.Items.Clear();

                    var month = Convert.ToInt32(cmbProductMonth.SelectedItem);
                    var year = Convert.ToInt32(cmbProductYear.SelectedItem);
                    var days = DateTime.DaysInMonth(year, month);
                    for (var i = 1; i <= days; i++) cmbProductDate.Items.Add(i);
                    cmbProductDate.SelectedIndex = 0;
                }

                break;
        }
    }

    private void cmbBirthYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateComboBox();
    }

    private void cmbProductYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateComboBox();
    }

    private void cmbProductMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateComboBox();
    }

    private void cmbProductDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        //UpdateComboBox();
    }

    private void btnViewEmployeeDetails_Click(object sender, EventArgs e)
    {
        if (_mode.Equals("ViewAccountUpdate"))
        {
            if (_rightID == 0 || dgvRightTable.DataSource == null || dgvRightTable.Rows.Count == 0)
            {
                btnViewEmployeeDetails.Enabled = false;
                return;
            }

            _db.Query = $@"SELECT Employee.*  FROM AccountUpdateHistory INNER JOIN
                Employee ON AccountUpdateHistory.EmployeeID = Employee.EmployeeID
                WHERE AccountUpdateHistory.AccountUpdateHistoryID = {_rightID}";
        }
        else
        {
            _db.Query = $@"SELECT * FROM Employee WHERE EmployeeID='{ID}'";
        }

        var info = _db.getEmployeeRecord()[0];
        var infoWindow = new frmEmployeeInfo(info);
        infoWindow.ShowDialog();
    }

    private void button3_Click_1(object sender, EventArgs e)
    {
        lblLeftTable.Text = "Account Updates";
        pnlTransactionOption.Visible = false;
        dgvRightTable.DataSource = null;
        pnlAddSomething.Visible = false;
        txtCashierSearch.Visible = false;
        lblSearchCashier.Visible = false;
        _mode = "ViewAccountUpdate";
        pnlRightTableOptions.Visible = true;
        lblRightTable.Text = "Account Changes";
        lblLoginCount.Visible = false;
        pnlAddUpdDeleteProduct.Visible = false;
        pnlReport.Visible = false;
        lblTotalPrice.Visible = false;
        lblRightTotalPrice.Visible = false;
        btnViewEmployeeDetails.Visible = true;
        pnlAddUpdDelProductOption.Visible = false;
    }

    private void dgvRightTable_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        try
        {
            _rightID = Convert.ToInt32(dgvRightTable.Rows[e.RowIndex].Cells[0].Value + "");
            if (_mode.Equals("ViewAccountUpdate")) btnViewEmployeeDetails.Enabled = true;
        }
        catch (Exception)
        {
        }
    }

    private void btnViewInventoryChanges_Click(object sender, EventArgs e)
    {
        lblLeftTable.Text = "Product Updates";
        pnlTransactionOption.Visible = false;
        dgvRightTable.DataSource = null;
        _mode = "ViewProductChanges";
        pnlRightTableOptions.Visible = true;
        txtCashierSearch.Visible = false;
        lblSearchCashier.Visible = false;
        lblRightTable.Text = "Product Changes";
        lblLoginCount.Visible = false;
        pnlAddUpdDeleteProduct.Visible = false;
        pnlReport.Visible = false;
        lblTotalPrice.Visible = false;
        lblRightTotalPrice.Visible = false;
        btnViewEmployeeDetails.Visible = true;
        pnlAddUpdDelProductOption.Visible = false;
    }

    private void chcbxShowPassword_CheckedChanged(object sender, EventArgs e)
    {
        if (chcbxShowPassword.Checked)
            txtPassword.PasswordChar = '\0';
        else
            txtPassword.PasswordChar = '*';
    }

    private void button4_Click(object sender, EventArgs e)
    {
        var exit = CloseWindow();
        if (exit)
        {
        }
    }

    private void btnAddNewCategory_Click(object sender, EventArgs e)
    {
        var categoryWindow = new AddNewCategory();
        categoryWindow.ShowDialog();
    }
}
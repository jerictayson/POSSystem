using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.MainWindows;
using Point_of_Sales_System.Windows.Pop_ups;

namespace Point_of_Sales_System;

public partial class Login : Form
{
    public static Login LoginInstance;

    private Database? _db;
    public bool isEmployeeLogin;

    public Login()
    {
        InitializeComponent();
    }

    public Employee EmployeeSession { get; set; }

    public bool GetLoginStatus
    {
        get => isEmployeeLogin;
        set => isEmployeeLogin = value;
    }

    public void CloseWindow()
    {
        try
        {
            Environment.Exit(0);
        }
        catch (Exception)
        {
            Dispose();
            Environment.Exit(0);
        }
    }

    private void button1_Click(object sender, EventArgs e)
    {
        var browse = new BrowseProduct(_db);
        browse.Show();
        Hide();
    }

    private void Login_Load(object sender, EventArgs e)
    {
        _db = new Database();
        if (_db.hasLoginError)
        {
            MessageBox.Show("Error Connecting on the Database Exiting...");
            Application.Exit();
        }
    }

    public void StartSession()
    {
        if (GetLoginStatus)
        {
            var dashboard = new CashierDashboard(EmployeeSession);
            dashboard.Show();
        }
    }

    private void Login_FormClosing(object sender, FormClosingEventArgs e)
    {
        CloseWindow();
    }

    public void ExitApplication()
    {
        Application.Exit();
    }

    private void button2_Click(object sender, EventArgs e)
    {
        var login = new EmployeeLogin(_db);
        login.Show();
        Hide();
    }
}
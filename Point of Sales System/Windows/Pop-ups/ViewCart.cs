﻿using Point_of_Sales_System.Model;
using Point_of_Sales_System.Windows.MainWindows;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class ViewCart : Form
{
    private readonly List<Product> _product;
    private readonly List<Product> _toRemoveProduct = new();
    private double _totalPrice;

    public ViewCart()
    {
        InitializeComponent();
    }

    public ViewCart(List<Product> products)
    {
        InitializeComponent();
        _product = products;
    }

    private void lblTotal_Click(object sender, EventArgs e)
    {
    }

    private void CloseWindow()
    {
        new BrowseProduct().Show();
        BrowseProduct.customerProducts = _product;
        Dispose();
    }

    private void UpdateList()
    {
        dgvCartList.ColumnCount = 4;
        dgvCartList.Columns[0].HeaderText = "Product ID";
        dgvCartList.Columns[1].HeaderText = "Product Name";
        dgvCartList.Columns[2].HeaderText = "Quantity";
        dgvCartList.Columns[3].HeaderText = "Price";
        foreach (var p in _product)
        {
            dgvCartList.Rows.Add(p.Id, p.ProductName,
                p.Quantity,
                p.Price);
            _totalPrice += p.Price;
        }

        lblTotal.Text = $"₱{_totalPrice}";
    }

    private void ViewCart_Load(object sender, EventArgs e)
    {
        UpdateList();
    }

    private void ViewCart_FormClosing(object sender, FormClosingEventArgs e)
    {
        CloseWindow();
    }

    private void btnDone_Click(object sender, EventArgs e)
    {
        CloseWindow();
    }

    private void dgvCartList_CellClick(object sender, DataGridViewCellEventArgs e)
    {
        _toRemoveProduct.Clear();
        if (dgvCartList.SelectedRows.Count > 0)
        {
            btnClearCart.Enabled = true;
            foreach (DataGridViewRow row in dgvCartList.SelectedRows)
            {
                var p = new Product();
                p.Id = Convert.ToInt32(row.Cells[0].Value + "");
                p.ProductName = row.Cells[1].Value + "";
                p.Quantity = Convert.ToInt32(row.Cells[2].Value + "");
                p.Price = (float)Convert.ToDouble(row.Cells[3].Value + "");
                _toRemoveProduct.Add(p);
            }
        }
        else
        {
            btnClearCart.Enabled = false;
        }
    }


    private void btnClearCart_Click(object sender, EventArgs e)
    {
        var res = MessageBox.Show("Are you sure you want to delete selected product?", "Confirmation",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        if (res == DialogResult.Yes)
        {
            var data = "";
            for (var i = 0; i < _product.Count; i++)
                foreach (var curr in _toRemoveProduct)
                    if (_product[i].Id == curr.Id)
                        _product.RemoveAt(i);

            foreach (DataGridViewRow selectedRow in dgvCartList.SelectedRows) dgvCartList.Rows.Remove(selectedRow);
        }
    }
}
﻿using System.Windows.Forms;

namespace Point_of_Sales_System.Windows.Pop_ups
{
    partial class EmployeeLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlEmailPane = new System.Windows.Forms.Panel();
            this.btnClearEmail = new System.Windows.Forms.Button();
            this.btnEnterEmail = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlPasswordLogin = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.pctrbxEmployee = new System.Windows.Forms.PictureBox();
            this.btnPasswordClear = new System.Windows.Forms.Button();
            this.btnPasswordLogin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblMessageStatus = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.pnlEmailPane.SuspendLayout();
            this.pnlPasswordLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlEmailPane
            // 
            this.pnlEmailPane.Controls.Add(this.btnClearEmail);
            this.pnlEmailPane.Controls.Add(this.btnEnterEmail);
            this.pnlEmailPane.Controls.Add(this.txtEmail);
            this.pnlEmailPane.Controls.Add(this.lblStatus);
            this.pnlEmailPane.Controls.Add(this.label2);
            this.pnlEmailPane.Controls.Add(this.label1);
            this.pnlEmailPane.Location = new System.Drawing.Point(0, 1);
            this.pnlEmailPane.Name = "pnlEmailPane";
            this.pnlEmailPane.Size = new System.Drawing.Size(362, 391);
            this.pnlEmailPane.TabIndex = 0;
            // 
            // btnClearEmail
            // 
            this.btnClearEmail.BackColor = System.Drawing.Color.Crimson;
            this.btnClearEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearEmail.FlatAppearance.BorderSize = 0;
            this.btnClearEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearEmail.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnClearEmail.Location = new System.Drawing.Point(95, 304);
            this.btnClearEmail.Name = "btnClearEmail";
            this.btnClearEmail.Size = new System.Drawing.Size(165, 36);
            this.btnClearEmail.TabIndex = 5;
            this.btnClearEmail.Text = "Clear";
            this.btnClearEmail.UseVisualStyleBackColor = false;
            this.btnClearEmail.Click += new System.EventHandler(this.btnClearEmail_Click);
            // 
            // btnEnterEmail
            // 
            this.btnEnterEmail.BackColor = System.Drawing.Color.ForestGreen;
            this.btnEnterEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEnterEmail.FlatAppearance.BorderSize = 0;
            this.btnEnterEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnterEmail.Image = global::Point_of_Sales_System.Properties.Resources.right_arrow;
            this.btnEnterEmail.Location = new System.Drawing.Point(95, 256);
            this.btnEnterEmail.Name = "btnEnterEmail";
            this.btnEnterEmail.Size = new System.Drawing.Size(165, 36);
            this.btnEnterEmail.TabIndex = 4;
            this.btnEnterEmail.UseVisualStyleBackColor = false;
            this.btnEnterEmail.Click += new System.EventHandler(this.btnEnterEmail_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.txtEmail.Location = new System.Drawing.Point(22, 222);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(315, 29);
            this.txtEmail.TabIndex = 3;
            this.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEmail.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEmail_KeyUp);
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(22, 133);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(315, 49);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Message status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(22, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(315, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter your user name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(66, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 71);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome Dear Employee!";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlPasswordLogin
            // 
            this.pnlPasswordLogin.Controls.Add(this.btnBack);
            this.pnlPasswordLogin.Controls.Add(this.lblEmployeeName);
            this.pnlPasswordLogin.Controls.Add(this.pctrbxEmployee);
            this.pnlPasswordLogin.Controls.Add(this.btnPasswordClear);
            this.pnlPasswordLogin.Controls.Add(this.btnPasswordLogin);
            this.pnlPasswordLogin.Controls.Add(this.txtPassword);
            this.pnlPasswordLogin.Controls.Add(this.lblMessageStatus);
            this.pnlPasswordLogin.Controls.Add(this.label4);
            this.pnlPasswordLogin.Controls.Add(this.lblWelcome);
            this.pnlPasswordLogin.Location = new System.Drawing.Point(0, 0);
            this.pnlPasswordLogin.Name = "pnlPasswordLogin";
            this.pnlPasswordLogin.Size = new System.Drawing.Size(362, 391);
            this.pnlPasswordLogin.TabIndex = 6;
            this.pnlPasswordLogin.Visible = false;
            // 
            // btnBack
            // 
            this.btnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.btnBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnBack.Location = new System.Drawing.Point(13, 11);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(89, 25);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "← Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.lblEmployeeName.Location = new System.Drawing.Point(95, 74);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(178, 27);
            this.lblEmployeeName.TabIndex = 7;
            this.lblEmployeeName.Text = "Welcome! ";
            this.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pctrbxEmployee
            // 
            this.pctrbxEmployee.Image = global::Point_of_Sales_System.Properties.Resources.user;
            this.pctrbxEmployee.Location = new System.Drawing.Point(131, 103);
            this.pctrbxEmployee.Name = "pctrbxEmployee";
            this.pctrbxEmployee.Size = new System.Drawing.Size(105, 68);
            this.pctrbxEmployee.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pctrbxEmployee.TabIndex = 6;
            this.pctrbxEmployee.TabStop = false;
            // 
            // btnPasswordClear
            // 
            this.btnPasswordClear.BackColor = System.Drawing.Color.Crimson;
            this.btnPasswordClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPasswordClear.FlatAppearance.BorderSize = 0;
            this.btnPasswordClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPasswordClear.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnPasswordClear.Location = new System.Drawing.Point(95, 329);
            this.btnPasswordClear.Name = "btnPasswordClear";
            this.btnPasswordClear.Size = new System.Drawing.Size(165, 36);
            this.btnPasswordClear.TabIndex = 5;
            this.btnPasswordClear.Text = "Clear";
            this.btnPasswordClear.UseVisualStyleBackColor = false;
            this.btnPasswordClear.Click += new System.EventHandler(this.btnPasswordClear_Click);
            // 
            // btnPasswordLogin
            // 
            this.btnPasswordLogin.BackColor = System.Drawing.Color.ForestGreen;
            this.btnPasswordLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPasswordLogin.FlatAppearance.BorderSize = 0;
            this.btnPasswordLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPasswordLogin.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnPasswordLogin.Location = new System.Drawing.Point(95, 287);
            this.btnPasswordLogin.Name = "btnPasswordLogin";
            this.btnPasswordLogin.Size = new System.Drawing.Size(165, 36);
            this.btnPasswordLogin.TabIndex = 4;
            this.btnPasswordLogin.Text = "Login";
            this.btnPasswordLogin.UseVisualStyleBackColor = false;
            this.btnPasswordLogin.Click += new System.EventHandler(this.btnPasswordLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.txtPassword.Location = new System.Drawing.Point(22, 253);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(315, 29);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPassword.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyUp);
            // 
            // lblMessageStatus
            // 
            this.lblMessageStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.lblMessageStatus.ForeColor = System.Drawing.Color.Red;
            this.lblMessageStatus.Location = new System.Drawing.Point(22, 173);
            this.lblMessageStatus.Name = "lblMessageStatus";
            this.lblMessageStatus.Size = new System.Drawing.Size(315, 47);
            this.lblMessageStatus.TabIndex = 2;
            this.lblMessageStatus.Text = "Message status";
            this.lblMessageStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMessageStatus.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(108, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Enter your Password:";
            // 
            // lblWelcome
            // 
            this.lblWelcome.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold);
            this.lblWelcome.Location = new System.Drawing.Point(74, 20);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(230, 46);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome! ";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EmployeeLogin
            // 
            this.AcceptButton = this.btnPasswordLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(361, 390);
            this.Controls.Add(this.pnlPasswordLogin);
            this.Controls.Add(this.pnlEmailPane);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "EmployeeLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeLogin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EmployeeLogin_FormClosing);
            this.Load += new System.EventHandler(this.EmployeeLogin_Load);
            this.pnlEmailPane.ResumeLayout(false);
            this.pnlEmailPane.PerformLayout();
            this.pnlPasswordLogin.ResumeLayout(false);
            this.pnlPasswordLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctrbxEmployee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel pnlEmailPane;
        private Label label1;
        private Button btnEnterEmail;
        private TextBox txtEmail;
        private Label lblStatus;
        private Label label2;
        private Button btnClearEmail;
        private Panel pnlPasswordLogin;
        private Label lblEmployeeName;
        private PictureBox pctrbxEmployee;
        private Button btnPasswordClear;
        private Button btnPasswordLogin;
        private TextBox txtPassword;
        private Label lblMessageStatus;
        private Label label4;
        private Label lblWelcome;
        private Button btnBack;
    }
}
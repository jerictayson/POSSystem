﻿
namespace Point_of_Sales_System.Windows.Pop_ups
{
    partial class Receipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.receiptReport = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // receiptReport
            // 
            this.receiptReport.ActiveViewIndex = -1;
            this.receiptReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.receiptReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.receiptReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receiptReport.Location = new System.Drawing.Point(0, 0);
            this.receiptReport.Name = "receiptReport";
            this.receiptReport.Size = new System.Drawing.Size(801, 450);
            this.receiptReport.TabIndex = 0;
            // 
            // Receipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 450);
            this.Controls.Add(this.receiptReport);
            this.Name = "Receipt";
            this.Text = "Receipt";
            this.Load += new System.EventHandler(this.Receipt_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer receiptReport;
    }
}
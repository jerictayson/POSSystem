﻿using System.Windows.Forms;

namespace Point_of_Sales_System.Windows.Pop_ups
{
    partial class frmOrderWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblOrder = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShopAgain = new System.Windows.Forms.Button();
            this.lblOrderNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(202, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order Received";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(206, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Here is your order ID:";
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOrder.Location = new System.Drawing.Point(205, 193);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(117, 30);
            this.lblOrder.TabIndex = 2;
            this.lblOrder.Text = "Order No:#";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(153, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(258, 51);
            this.label4.TabIndex = 3;
            this.label4.Text = "Wait for the cashier to call your order id to\r\n             complete your transac" +
    "tion \r\n                         Thank you!!\r\n";
            // 
            // btnShopAgain
            // 
            this.btnShopAgain.BackColor = System.Drawing.Color.ForestGreen;
            this.btnShopAgain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnShopAgain.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnShopAgain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShopAgain.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnShopAgain.Location = new System.Drawing.Point(187, 314);
            this.btnShopAgain.Name = "btnShopAgain";
            this.btnShopAgain.Size = new System.Drawing.Size(197, 53);
            this.btnShopAgain.TabIndex = 4;
            this.btnShopAgain.Text = "Shop Again!";
            this.btnShopAgain.UseVisualStyleBackColor = false;
            this.btnShopAgain.Click += new System.EventHandler(this.btnShopAgain_Click);
            // 
            // lblOrderNumber
            // 
            this.lblOrderNumber.AutoSize = true;
            this.lblOrderNumber.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOrderNumber.Location = new System.Drawing.Point(316, 193);
            this.lblOrderNumber.Name = "lblOrderNumber";
            this.lblOrderNumber.Size = new System.Drawing.Size(53, 30);
            this.lblOrderNumber.TabIndex = 5;
            this.lblOrderNumber.Text = "lablr";
            // 
            // frmOrderWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.ClientSize = new System.Drawing.Size(555, 445);
            this.Controls.Add(this.lblOrderNumber);
            this.Controls.Add(this.btnShopAgain);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblOrder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmOrderWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderReceived!";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOrderWindow_FormClosing);
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label lblOrder;
        private Label label4;
        private Button btnShopAgain;
        private Label lblOrderNumber;
    }
}
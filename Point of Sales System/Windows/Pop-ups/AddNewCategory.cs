﻿using System.Data;
using Point_of_Sales_System.Services;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class AddNewCategory : Form
{
    private readonly Database _db = new();
    private string _mode = "";

    public AddNewCategory()
    {
        InitializeComponent();
    }

    private void label2_Click(object sender, EventArgs e)
    {
    }

    private void label8_Click(object sender, EventArgs e)
    {
    }

    private void AddNewCategory_Load(object sender, EventArgs e)
    {
        _db.Query = @"SELECT CategoryID AS 'CategoryID', [Category Name] AS 'CategoryName' 
                                FROM Category";

        cmbCategory.ValueMember = "CategoryName";
        cmbCategory.DisplayMember = "CategoryName";
        cmbCategory.DataSource = _db.getTableData("Category").Tables[0];
        cmbCategory.SelectedIndex = 0;
        rdbAdd.Checked = true;
    }

    private void btnAddCategory_Click(object sender, EventArgs e)
    {
        var category = txtCategory.Text.Trim().ToLower();
        if (string.IsNullOrEmpty(category))
        {
            MessageBox.Show("Input cannot be empty.");
            return;
        }

        switch (_mode)
        {
            case "Add":

                var message = "";
                _db.Query = @"SELECT [Category Name] FROM Category";
                foreach (DataRow row in _db.getTableData("Category").Tables[0].Rows)
                {
                    message = @$"{row[0].ToString().ToLower().Trim()}";
                    if (category.Equals(message))
                    {
                        MessageBox.Show("Category already exists, please choose another.");
                        return;
                    }
                }

                _db.Query = $@"INSERT INTO Category VALUES ('{category}')";
                _db.AddUpdateDeleteRecord();
                MessageBox.Show("Category Added!");
                break;
            case "Update":
                var oldValue = cmbCategory.Text.Trim();
                _db.Query = $@"UPDATE Category SET [Category Name]='{category}' WHERE [Category Name]='{oldValue}'";
                _db.AddUpdateDeleteRecord();
                MessageBox.Show("Category Updated!");
                break;
            case "Delete":
                var res = MessageBox.Show("Are you sure you want to delete this category?", "Confirmation",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation);
                if (res == DialogResult.Yes)
                {
                    _db.Query = $@"DELETE FROM Category WHERE [Category Name]='{category}'";
                    _db.AddUpdateDeleteRecord();
                    MessageBox.Show("Category Deleted!");
                }

                break;
        }
    }

    private void rdbAdd_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbAdd.Checked)
        {
            _mode = "Add";
            cmbCategory.SelectedIndex = 0;
            txtCategory.Text = "";
            cmbCategory.Enabled = false;
            txtCategory.Enabled = true;
        }
    }

    private void rdbUpdate_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbUpdate.Checked)
        {
            _mode = "Update";
            txtCategory.Text = "";
            cmbCategory.SelectedIndex = 0;
            txtCategory.Enabled = true;
            cmbCategory.Enabled = true;
        }
    }

    private void rdbDelete_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbDelete.Checked)
        {
            _mode = "Delete";

            cmbCategory.SelectedIndex = 0;
            cmbCategory.Enabled = true;
            txtCategory.Text = "";
            txtCategory.Enabled = false;
            txtCategory.Enabled = true;
        }
    }
}
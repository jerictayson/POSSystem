﻿using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.MainWindows;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class ViewOrderWindow : Form
{
    private double _customerMoney;
    private readonly Database _db = new();
    private readonly int _orderId;
    private double _totalPrice, exchange;

    public ViewOrderWindow()
    {
        InitializeComponent();
    }

    public ViewOrderWindow(int orderId)
    {
        _orderId = orderId;
        InitializeComponent();
    }

    private void btnProcessOrder_Click(object sender, EventArgs e)
    {
    }

    private void CloseWindow()
    {
        new CashierDashboard().Show();
        Dispose();
    }

    private void ViewOrderWindow_Load(object sender, EventArgs e)
    {
        _db.Query = $@"SELECT OrderDetails.ProductID, Product.[Product Name],
                        Product.Price, Quantity, Total FROM OrderDetails INNER JOIN 
                        Product ON Product.ProductID = OrderDetails.ProductID WHERE OrderID='{_orderId}';";
        dgvProductOrders.DataSource = _db.getTableData("OrderDetails").Tables[0];

        foreach (DataGridViewRow row in dgvProductOrders.Rows) _totalPrice += Convert.ToDouble(row.Cells[2].Value + "");
        lblTotalPrice.Text = _totalPrice.ToString("#.##");
    }

    private void label9_Click(object sender, EventArgs e)
    {
    }

    private void ViewOrderWindow_FormClosing(object sender, FormClosingEventArgs e)
    {
        CloseWindow();
    }

    private void txtCustomerMoney_KeyUp(object sender, KeyEventArgs e)
    {
        try
        {
            _customerMoney = Convert.ToDouble(txtCustomerMoney.Text.Trim());
            if (_customerMoney > _totalPrice) btnProcessOrder.Enabled = true;
            exchange = _customerMoney - _totalPrice;
            lblExchange.Text = exchange.ToString("#.##");
        }
        catch (Exception ex)
        {
            btnProcessOrder.Enabled = false;
        }
    }
}
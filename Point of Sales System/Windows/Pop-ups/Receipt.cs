﻿using System.Data;
using CrystalDecisions.CrystalReports.Engine;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class Receipt : Form
{
    public Receipt()
    {
        InitializeComponent();
    }

    public static string XMLPath { get; set; }

    public static DataSet DataSet1 { get; set; }

    private void Receipt_Load(object sender, EventArgs e)
    {
        var reportDocument = new ReportDocument();
        reportDocument.Load(XMLPath);
        reportDocument.SetDataSource(DataSet1.Tables[0]);

        receiptReport.ReportSource = reportDocument;
    }
}
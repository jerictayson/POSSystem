﻿using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.MainWindows;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class frmOrderWindow : Form
{
    public static bool _isSuccess = true;

    private readonly Database _db = new();
    private double _totalPrice;

    public frmOrderWindow()
    {
        InitializeComponent();
        AssignCashier();
    }

    private void Form3_Load(object sender, EventArgs e)
    {
        foreach (var item in BrowseProduct.customerProducts) _totalPrice += item.Price;

        //AssignCashier();
        BrowseProduct.customerProducts.Clear();
    }

    private void AssignCashier()
    {
        _db.Query = @"SELECT Employee.EmployeeID, [Profile Picture], [First Name],
                        [Middle Name], [Last Name], Address, Gender, [Birth Date], Employee.[User Name], EmployeeType, AccountStatus
                        FROM Employee INNER JOIN EmployeeCredential ON EmployeeCredential.[User Name] = Employee.[User Name]
                        WHERE EmployeeType='Cashier' AND EmployeeCredential.LoggedIn='1' AND AccountStatus='Enable'";
        var assignedCashier = getCashier(_db.getEmployeeRecord());
        if (assignedCashier == 0) return;
        _db.Query =
            $@"INSERT INTO CustomerOrder(EmployeeID, [Transaction Date], Status, [Total Price]) VALUES ({assignedCashier}, '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}', 'Pending', '{_totalPrice}')";
        _db.AddUpdateDeleteRecord();
        _db.Query =
            "SELECT [Order ID] FROM CustomerOrder WHERE [Transaction Date] IN (SELECT MAX([Transaction Date]) FROM CustomerOrder)";
        var latestRecord = _db.GetLatestRecord(0);
        foreach (var product in BrowseProduct.customerProducts)
        {
            _db.Query = $@"INSERT INTO OrderDetails (OrderID, ProductID, Quantity, Total)
                            VALUES (
                            {latestRecord},
                            {product.Id},
                            {product.Quantity},
                            {product.Price}
                            )";
            _db.AddUpdateDeleteRecord();
        }

        lblOrderNumber.Text = latestRecord;
        _totalPrice = 0;
        BrowseProduct.customerProducts.Clear();
    }

    private int getCashier(List<Employee> employees)
    {
        try
        {
            var rand = new Random();
            var id = employees[rand.Next(employees.Count)].Id;
            return id;
        }
        catch (NullReferenceException)
        {
            MessageBox.Show("There are no online cashier on the system, cannot be continue.");
            _isSuccess = false;
            return 0;
        }
    }

    private void CloseWindow()
    {
        new BrowseProduct().Show();
        Dispose();
    }

    private void btnShopAgain_Click(object sender, EventArgs e)
    {
        CloseWindow();
    }

    private void frmOrderWindow_FormClosing(object sender, FormClosingEventArgs e)
    {
        CloseWindow();
    }
}
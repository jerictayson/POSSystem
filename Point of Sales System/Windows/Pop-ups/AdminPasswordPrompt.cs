﻿using Point_of_Sales_System.Services;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class frmPasswordPrompt : Form
{
    private readonly Database _db = new();

    public frmPasswordPrompt()
    {
        InitializeComponent();
    }


    public bool canEdit { get; set; }


    private void button1_Click(object sender, EventArgs e)
    {
        CloseWindow();
    }

    private void btnProceed_Click(object sender, EventArgs e)
    {
        var userName = txtUserName.Text.Trim();
        var password = txtPassword.Text.Trim();
        if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
        {
            MessageBox.Show("Please fill out all the fields");
            return;
        }

        _db.Query = $@"SELECT [User Name], Password, LoggedIn FROM EmployeeCredential WHERE [User Name]='{userName}';";
        var data = _db.GetEmployee("EmployeeCredential");
        if (data == null)
        {
            MessageBox.Show("Invalid User name");
        }
        else
        {
            var passwordAcc = data.Password;
            if (passwordAcc.Equals(password))
            {
                canEdit = true;
                Close();
            }
            else
            {
                MessageBox.Show("Invalid Password.");
            }
        }
    }

    private void CloseWindow()
    {
    }

    private void frmCheckoutWindow_FormClosing(object sender, FormClosingEventArgs e)
    {
    }

    private void frmCheckoutWindow_Load(object sender, EventArgs e)
    {
    }
}
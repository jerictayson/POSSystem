﻿using Point_of_Sales_System.Model;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class frmEmployeeInfo : Form
{
    private readonly Employee _employee;

    public frmEmployeeInfo()
    {
        InitializeComponent();
    }

    public frmEmployeeInfo(Employee info)
    {
        _employee = info;
        InitializeComponent();
    }

    private void pictureBox2_Click(object sender, EventArgs e)
    {
    }

    private void lblEmployeeName_Click(object sender, EventArgs e)
    {
    }

    private void label4_Click(object sender, EventArgs e)
    {
    }

    private void frmEmployeeInfo_Load(object sender, EventArgs e)
    {
        lblEmployeeName.Text = $"{_employee.FirstName} {_employee.LastName}";
        lblEmployeeType.Text = $"{_employee.EmployeeType}";
        lblAddress.Text = $"{_employee.Address}";
        lblGender.Text = $"{_employee.Gender}";
        _employee.BirthDate = _employee.BirthDate.Substring(0, _employee.BirthDate.IndexOf(" ")).Trim();
        lblBirthDate.Text = $"{_employee.BirthDate}";
        lblUserName.Text = $"{_employee.UserName}";
    }
}
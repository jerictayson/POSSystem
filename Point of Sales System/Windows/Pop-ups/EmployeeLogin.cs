﻿using Point_of_Sales_System.Model;
using Point_of_Sales_System.Services;
using Point_of_Sales_System.Windows.MainWindows;

namespace Point_of_Sales_System.Windows.Pop_ups;

public partial class EmployeeLogin : Form
{
    private int _attempts = 3;
    private readonly Database? _db;
    private string _email = null!, _password = null!;
    private bool _isLogin;

    public EmployeeLogin()
    {
        InitializeComponent();
    }

    public EmployeeLogin(Database? db)
    {
        InitializeComponent();
        this._db = db;
    }

    public Employee Employee { get; set; } = null!;

    private void btnEnterEmail_Click(object sender, EventArgs e)
    {
        _email = txtEmail.Text.Trim().ToLower();
        _db.Query = $@"SELECT [User Name], Password, LoggedIn FROM EmployeeCredential WHERE [User Name]='{_email}';";
        Employee = _db.GetEmployee("EmployeeCredential");
        if (!Employee.IsLoggedIn)
        {
            lblStatus.Visible = false;
            _password = Employee.Password;
            _db.Query = $@"SELECT [Profile Picture] FROM Employee WHERE [User Name]='{_email}'";
            Employee.ProfilePicture = _db.GetEmployee("Employee").ProfilePicture;
            if (!string.IsNullOrEmpty(Employee.ProfilePicture))
                pctrbxEmployee.Image = Image.FromFile(Employee.ProfilePicture);
            _db.Query = $@"SELECT * FROM Employee WHERE [User Name]='{_email}'";
            Employee = _db.getEmployeeRecord()[0];
            if (Employee.AccountStatus.Equals("Enable"))
            {
                lblEmployeeName.Text = $@"{Employee.FirstName} {Employee.LastName}";
                pnlEmailPane.Hide();
                pnlPasswordLogin.Show();
                lblMessageStatus.Visible = false;
                btnPasswordLogin.Enabled = true;
                txtPassword.Enabled = true;
            }
            else
            {
                lblStatus.Text = @"Your account is disabled, Please contact your administrator.";
                lblStatus.Visible = true;
            }
        }
        else
        {
            MessageBox.Show(@"Account is already logged in.");
        }
    }

    private void btnClearEmail_Click(object sender, EventArgs e)
    {
        txtEmail.Text = "";
    }

    private void btnPasswordLogin_Click(object sender, EventArgs e)
    {
        var passwordInput = txtPassword.Text.Trim();
        if (passwordInput.Equals(_password))
        {
            _db.Query = $@"SELECT * FROM Employee WHERE [User Name]='{_email}'";
            Employee = _db.getEmployeeRecord()[0];
            if (!Employee.IsLoggedIn)
            {
                _isLogin = true;
                lblMessageStatus.Visible = false;
                CloseWindow();
            }
            else
            {
                MessageBox.Show(@"This account is already logged in.");
                _isLogin = false;
            }
        }
        else
        {
            if (_attempts == 0)
            {
                lblMessageStatus.Text = @"Your account is now disabled, Please contact your administrator.";
                txtPassword.Text = "";
                _db.Query = $@"UPDATE Employee SET AccountStatus='Disable' WHERE [User Name]='{Employee.UserName}'";
                _db.AddUpdateDeleteRecord();
                txtPassword.Enabled = false;
                btnPasswordLogin.Enabled = false;
            }
            else
            {
                _attempts--;
                lblMessageStatus.Text = $@"Invalid Password. {_attempts + 1} Remaining.";
            }

            lblMessageStatus.Visible = true;
            _isLogin = false;
        }
    }

    private bool CloseWindow()
    {
        if (IsLoginSuccess())
        {
            CashierDashboard dashboard;
            AdminDashboard admin;
            Employee.IsLoggedIn = true;
            _db.Query = $@"UPDATE EmployeeCredential SET LoggedIn={Convert.ToInt32(Employee.IsLoggedIn)}
                            WHERE [User Name] = '{Employee.UserName}'";
            _db.AddUpdateDeleteRecord();
            if (Employee.EmployeeType.Equals("Admin"))
            {
                admin = new AdminDashboard(Employee, _db);
                admin.Show();
            }
            else if (Employee.EmployeeType.Equals("Cashier"))
            {
                dashboard = new CashierDashboard(Employee);
                dashboard.Show();
            }

            _db.Query = $@"INSERT INTO LoginHistory VALUES ('{Employee.UserName}', 
                '{DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}')";
            _db.AddUpdateDeleteRecord();
            Dispose();
        }
        else
        {
            var res = MessageBox.Show(@"Are you sure to quit application?",
                @"Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                var login = new Login();
                login.ExitApplication();
            }
        }


        return true;
    }

    private void EmployeeLogin_FormClosing(object sender, FormClosingEventArgs e)
    {
        e.Cancel = CloseWindow();
    }

    private void btnPasswordClear_Click(object sender, EventArgs e)
    {
        txtPassword.Text = "";
    }

    private void btnBack_Click(object sender, EventArgs e)
    {
        txtEmail.Text = "";
        pnlPasswordLogin.Hide();
        pnlEmailPane.Show();
    }

    private void txtPassword_KeyUp(object sender, KeyEventArgs e)
    {
        //lblMessageStatus.Text = "";
    }

    private void txtEmail_KeyUp(object sender, KeyEventArgs e)
    {
        lblStatus.Text = "";
    }

    private void EmployeeLogin_Load(object sender, EventArgs e)
    {
    }

    public bool IsLoginSuccess()
    {
        return _isLogin;
    }
}
﻿namespace Point_of_Sales_System.Model;

public class Product
{
    public int Id { get; set; }

    public string ProductName { get; set; }


    public double Price { get; set; }

    public int Quantity { get; set; }

    public string ProductPicture { get; set; }

    public string Description { get; set; }

    public string Expiration_Date { get; set; }

    public string CategoryName { get; set; }

    public int Stocks { get; set; }
}
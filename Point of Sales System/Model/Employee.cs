﻿namespace Point_of_Sales_System.Model;

public class Employee
{
    public int Id { get; set; }
    public string FirstName { get; set; }

    public string ProfilePicture { get; set; }

    public string MiddleName { get; set; }

    public string LastName { get; set; }

    public string Address { get; set; }

    public string Gender { get; set; }

    public string BirthDate { get; set; }

    public string EmployeeEmail { get; set; }

    public string EmployeeType { get; set; }

    public string UserName { get; set; }
    public string Password { get; set; }

    public string AccountStatus { get; set; }

    public bool IsLoggedIn { get; set; }
}
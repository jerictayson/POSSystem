# Point of Sales System School Project

**NOTE**
This project is only for  demonstration of POSSystem. It cannot be used as a System to a 
business. This project is for education only and cannot be used as a production system.

## Pre-requisite
* Microsoft Visual Studio 2019/2022
* Crystal Report (2022)
* Microsoft SQL Server
* Microsoft SQL Server Management Studio
* .Net Framework 4.8

## How to run
1. Run the database script to [MSSQL](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) **(Note: if you are using sql server authentication, you may need to configure your settings by enable TCP/IP Connection to your setup on SQL server configuration manager in order it to work.)**
2. Modify the source code of the program (The Database.cs *connectionString*) and set appropriate credential for your setup.
3. Run the program.


## Screenshot
![Screenshot1](Point%20of%20Sales%20System/Assets/images/screenshots/screenshot1.PNG)
![Screenshot2](Point%20of%20Sales%20System/Assets/images/screenshots/screenshot2.PNG)
![Screenshot3](Point%20of%20Sales%20System/Assets/images/screenshots/screenshot3.PNG)
![Screenshot4](Point%20of%20Sales%20System/Assets/images/screenshots/screenshot4.PNG)
![Screenshot5](Point%20of%20Sales%20System/Assets/images/screenshots/screenshot5.PNG)

## Attribution
### Flaticons

#### Developers
Jeric Tayson\
Ronaldo Orantia\
Erika Santos\
Rhaymond De Jesus
